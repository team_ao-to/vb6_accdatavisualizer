Attribute VB_Name = "Module_TruckStructure"
'-----------------------------------------------------------------------------------------------------------------------
'
' (c) Copyright 2016 by Ao-to Corporation. All rights reserved.
'
' The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
' No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
' the specific written permission signed by a legal representative of the Ao-to Corporation.
'
' Change log:
'
' ----------     -----------     -------------     ---------------------------------------------------------------------
' Date           Username        Ticket            Change description
' ----------     -----------     -------------     ---------------------------------------------------------------------
' 19.10.2016     razvan          AOTO2             Initial version of the file
'-----------------------------------------------------------------------------------------------------------------------

' Particles in the truck
Public Const cTruckStructure_ParticleFrontLeftWheel = 1
Public Const cTruckStructure_ParticleRearLeftWheel = 2
Public Const cTruckStructure_ParticleFrontRightWheel = 3
Public Const cTruckStructure_ParticleRearRightWheel = 4
Public Const cTruckStructure_ParticleChassis = 5
Public Const cTruckStructure_ParticleCabin = 6
Public Const cTruckStructure_ParticleMax = 6

'Configurable dimensions of the truck
Public Const cTruckStructure_DimChassisOrigX = 0
Public Const cTruckStructure_DimChassisOrigZ = 0

Public Const cTruckStructure_DimChassisHeight = 80
Public Const cTruckStructure_DimChassisWidth = 200
Public Const cTruckStructure_DimFrontWheelOffsetToChassisX = 100
Public Const cTruckStructure_DimEcartment = 330
Public Const cTruckStructure_DimRearWheelToChassisXEnd = 110
Public Const cTruckStructure_DimWheelRadius = 58
Public Const cTruckStructure_DimCenterMassOffsetToChassisX = -180
Public Const cTruckStructure_DimCenterMassOffsetToChassisY = -90

Public Const cTruckStructure_DimJointOffsetToChassisY = 30
Public Const cTruckStructure_DimFrontSpringDefaultLen = 0.25
Public Const cTruckStructure_DimRearSpringDefaultLen = 0.25

Public Const cTruckStructure_DimCabinHeight = 200
Public Const cTruckStructure_DimCabinLength = 250
Public Const cTruckStructure_DimCabinWidth = 200
Public Const cTruckStructure_DimCenterMassOffsetToCabinX = -125
Public Const cTruckStructure_DimCenterMassOffsetToCabinY = -100
Public Const cTruckStructure_DimCabinOffsetToChassisY = 10

'-----------------------------------------------------------------------------------
'Non configurable dimensions of the truck. Computed based on the configurable ones
'-----------------------------------------------------------------------------------

Public Const cTruckStructure_DimChassisOrigY = cTruckStructure_DimWheelRadius + cTruckStructure_DimFrontSpringDefaultLen * 100 - cTruckStructure_DimJointOffsetToChassisY
Public Const cTruckStructure_DimChassisLength = cTruckStructure_DimFrontWheelOffsetToChassisX + cTruckStructure_DimEcartment + cTruckStructure_DimRearWheelToChassisXEnd

'Chassis joints related constants
Public Const cTruckStructure_DimChassisFrontJointsOffsetX = cTruckStructure_DimCenterMassOffsetToChassisX + cTruckStructure_DimFrontWheelOffsetToChassisX
Public Const cTruckStructure_DimChassisRearJointOffsetX = cTruckStructure_DimCenterMassOffsetToChassisX + cTruckStructure_DimFrontWheelOffsetToChassisX + cTruckStructure_DimEcartment
Public Const cTruckStructure_DimChassisAllJointsOffsetY = cTruckStructure_DimCenterMassOffsetToChassisY + cTruckStructure_DimJointOffsetToChassisY

'Cabin joints related constants
Public Const cTruckStructure_DimCabinFrontJointsOffsetX = -cTruckStructure_DimCabinLength / 2
Public Const cTruckStructure_DimCabinRearJointOffsetX = cTruckStructure_DimCabinLength / 2
Public Const cTruckStructure_DimCabinAllJointsOffsetY = -cTruckStructure_DimCabinHeight / 2

'Init values for chassis
Public Const cTruckStructure_DimChassisFrontJointsPosYInit = cTruckStructure_DimWheelRadius + cTruckStructure_DimFrontSpringDefaultLen * 100
Public Const cTruckStructure_DimChassisRearJointPosYInit = cTruckStructure_DimWheelRadius + cTruckStructure_DimRearSpringDefaultLen * 100

'Init values for cabin
Public Const cTruckStructure_DimCabinFrontJointsPosYInit = 160
Public Const cTruckStructure_DimCabinRearJointPosYInit = 160

Public Sub Truck_Init()
    Dim i
    
    ' Initialize all parts of the truck
    For i = 1 To cTruckStructure_ParticleMax
        Particle_Init aParticleList(i)
    Next
   
    ' Set front left wheel
    aParticleList(cTruckStructure_ParticleFrontLeftWheel).posCenterMass.X = cTruckStructure_DimChassisOrigX + cTruckStructure_DimFrontWheelOffsetToChassisX
    aParticleList(cTruckStructure_ParticleFrontLeftWheel).posCenterMass.Y = cTruckStructure_DimWheelRadius
    aParticleList(cTruckStructure_ParticleFrontLeftWheel).posCenterMass.Z = -cTruckStructure_DimChassisWidth / 2
    Draw_AddShapeWheelToParticle aParticleList(cTruckStructure_ParticleFrontLeftWheel), 7, 35, cTruckStructure_DimWheelRadius, 20, vbBlue
    
    ' Set rear left wheel
    aParticleList(cTruckStructure_ParticleRearLeftWheel).posCenterMass.X = cTruckStructure_DimChassisOrigX + cTruckStructure_DimFrontWheelOffsetToChassisX + cTruckStructure_DimEcartment
    aParticleList(cTruckStructure_ParticleRearLeftWheel).posCenterMass.Y = cTruckStructure_DimWheelRadius
    aParticleList(cTruckStructure_ParticleRearLeftWheel).posCenterMass.Z = -cTruckStructure_DimChassisWidth / 2
    Draw_AddShapeWheelToParticle aParticleList(cTruckStructure_ParticleRearLeftWheel), 7, 35, cTruckStructure_DimWheelRadius, 20, vbBlue
    
    ' Set front right wheel
    aParticleList(cTruckStructure_ParticleFrontRightWheel).posCenterMass.X = cTruckStructure_DimChassisOrigX + cTruckStructure_DimFrontWheelOffsetToChassisX
    aParticleList(cTruckStructure_ParticleFrontRightWheel).posCenterMass.Y = cTruckStructure_DimWheelRadius
    aParticleList(cTruckStructure_ParticleFrontRightWheel).posCenterMass.Z = cTruckStructure_DimChassisWidth / 2
    Draw_AddShapeWheelToParticle aParticleList(cTruckStructure_ParticleFrontRightWheel), 7, 35, cTruckStructure_DimWheelRadius, 20, vbBlue
    
    ' Set rear right wheel
    aParticleList(cTruckStructure_ParticleRearRightWheel).posCenterMass.X = cTruckStructure_DimChassisOrigX + cTruckStructure_DimFrontWheelOffsetToChassisX + cTruckStructure_DimEcartment
    aParticleList(cTruckStructure_ParticleRearRightWheel).posCenterMass.Y = cTruckStructure_DimWheelRadius
    aParticleList(cTruckStructure_ParticleRearRightWheel).posCenterMass.Z = cTruckStructure_DimChassisWidth / 2
    Draw_AddShapeWheelToParticle aParticleList(cTruckStructure_ParticleRearRightWheel), 7, 35, cTruckStructure_DimWheelRadius, 20, vbBlue
    
    ' Set chassis
    aParticleList(cTruckStructure_ParticleChassis).mass = 6000
    ' Set drawable shape for chassis
    Draw_AddShapeCubeToParticle aParticleList(cTruckStructure_ParticleChassis), cTruckStructure_DimChassisLength, cTruckStructure_DimChassisWidth, cTruckStructure_DimChassisHeight, cTruckStructure_DimCenterMassOffsetToChassisX, cTruckStructure_DimCenterMassOffsetToChassisY, vbGreen
    ' Set the chassis joints offsets to the center of mass point
    Particle_SetJointOffsets aParticleList(cTruckStructure_ParticleChassis), cTruckStructure_DimCabinWidth, cTruckStructure_DimChassisFrontJointsOffsetX, cTruckStructure_DimChassisRearJointOffsetX, cTruckStructure_DimChassisAllJointsOffsetY
    ' Set the initial 3 joints positions
    Particle_SetJointPosition aParticleList(cTruckStructure_ParticleChassis), gJointType_FrontLeft_c, cTruckStructure_DimChassisOrigX + cTruckStructure_DimFrontWheelOffsetToChassisX, cTruckStructure_DimChassisFrontJointsPosYInit, -cTruckStructure_DimChassisWidth / 2
    Particle_SetJointPosition aParticleList(cTruckStructure_ParticleChassis), gJointType_FrontRight_c, cTruckStructure_DimChassisOrigX + cTruckStructure_DimFrontWheelOffsetToChassisX, cTruckStructure_DimChassisFrontJointsPosYInit, cTruckStructure_DimChassisWidth / 2
    Particle_SetJointPosition aParticleList(cTruckStructure_ParticleChassis), gJointType_Rear_c, cTruckStructure_DimChassisOrigX + cTruckStructure_DimFrontWheelOffsetToChassisX + cTruckStructure_DimEcartment, cTruckStructure_DimChassisRearJointPosYInit, 0

    ' Set cabin
    aParticleList(cTruckStructure_ParticleCabin).mass = 1000
    ' Set drawable shape for cabin
    Draw_AddShapeCubeToParticle aParticleList(cTruckStructure_ParticleCabin), cTruckStructure_DimCabinLength, cTruckStructure_DimCabinWidth, cTruckStructure_DimCabinHeight, cTruckStructure_DimCenterMassOffsetToCabinX, cTruckStructure_DimCenterMassOffsetToCabinY, vbBlue
    ' Set the cabin joints offsets to the center of mass point
    Particle_SetJointOffsets aParticleList(cTruckStructure_ParticleCabin), cTruckStructure_DimCabinWidth, cTruckStructure_DimCabinFrontJointsOffsetX, cTruckStructure_DimCabinRearJointOffsetX, cTruckStructure_DimCabinAllJointsOffsetY
    ' Set the initial 3 joints positions
    Particle_SetJointPosition aParticleList(cTruckStructure_ParticleCabin), gJointType_FrontLeft_c, cTruckStructure_DimChassisOrigX, cTruckStructure_DimCabinFrontJointsPosYInit, -cTruckStructure_DimChassisWidth / 2
    Particle_SetJointPosition aParticleList(cTruckStructure_ParticleCabin), gJointType_FrontRight_c, cTruckStructure_DimChassisOrigX, cTruckStructure_DimCabinFrontJointsPosYInit, cTruckStructure_DimChassisWidth / 2
    Particle_SetJointPosition aParticleList(cTruckStructure_ParticleCabin), gJointType_Rear_c, cTruckStructure_DimChassisOrigX + cTruckStructure_DimCabinLength, cTruckStructure_DimCabinRearJointPosYInit, 0
    
    ' Set front left wheel chassis spring
    dampedSpringList(cDampedSpring_FlChassis).basePos.idxParticle = cTruckStructure_ParticleFrontLeftWheel
    dampedSpringList(cDampedSpring_FlChassis).basePos.jointType = gJointType_FrontLeft_c
    
    ' Set front right wheel chassis spring
    dampedSpringList(cDampedSpring_FrChassis).basePos.idxParticle = cTruckStructure_ParticleFrontRightWheel
    dampedSpringList(cDampedSpring_FrChassis).basePos.jointType = gJointType_FrontRight_c
    
    ' Set rear chassis spring
    dampedSpringList(cDampedSpring_RChassis).basePos.idxParticle = cTruckStructure_ParticleRearLeftWheel
    dampedSpringList(cDampedSpring_RChassis).basePos.offset.Z = cTruckStructure_DimChassisWidth / 2
    dampedSpringList(cDampedSpring_RChassis).basePos.jointType = gJointType_Rear_c
    
        
    ' Set front left cabin spring
    dampedSpringList(cDampedSpring_FlCabin).basePos.idxParticle = cTruckStructure_ParticleChassis
    dampedSpringList(cDampedSpring_FlCabin).basePos.offset.X = cTruckStructure_DimCenterMassOffsetToChassisX
    dampedSpringList(cDampedSpring_FlCabin).basePos.offset.Y = cTruckStructure_DimCenterMassOffsetToChassisY + cTruckStructure_DimChassisHeight
    dampedSpringList(cDampedSpring_FlCabin).basePos.offset.Z = -cTruckStructure_DimCabinWidth / 2
    dampedSpringList(cDampedSpring_FlCabin).basePos.jointType = gJointType_FrontLeft_c
    
    
    ' Set front right cabin spring
    dampedSpringList(cDampedSpring_FrCabin).basePos.idxParticle = cTruckStructure_ParticleChassis
    dampedSpringList(cDampedSpring_FrCabin).basePos.offset.X = cTruckStructure_DimCenterMassOffsetToChassisX
    dampedSpringList(cDampedSpring_FrCabin).basePos.offset.Y = cTruckStructure_DimCenterMassOffsetToChassisY + cTruckStructure_DimChassisHeight
    dampedSpringList(cDampedSpring_FrCabin).basePos.offset.Z = cTruckStructure_DimCabinWidth / 2
    dampedSpringList(cDampedSpring_FrCabin).basePos.jointType = gJointType_FrontRight_c
    
    
    ' Set rear cabin spring
    dampedSpringList(cDampedSpring_RCabin).basePos.idxParticle = cTruckStructure_ParticleChassis
    dampedSpringList(cDampedSpring_RCabin).basePos.offset.X = cTruckStructure_DimCenterMassOffsetToChassisX + cTruckStructure_DimCabinLength
    dampedSpringList(cDampedSpring_RCabin).basePos.offset.Y = cTruckStructure_DimCenterMassOffsetToChassisY + cTruckStructure_DimChassisHeight
    dampedSpringList(cDampedSpring_RCabin).basePos.offset.Z = 0
    dampedSpringList(cDampedSpring_RCabin).basePos.jointType = gJointType_Rear_c
        
        
    Particle_SetJointPosition aParticleList(cTruckStructure_ParticleChassis), gJointType_FrontLeft_c, aParticleList(cTruckStructure_ParticleFrontLeftWheel).posCenterMass.X, aParticleList(cTruckStructure_ParticleFrontLeftWheel).posCenterMass.Y + dampedSpringList(cDampedSpring_FlChassis).length * 100, -cTruckStructure_DimChassisWidth / 2
    Particle_SetJointPosition aParticleList(cTruckStructure_ParticleChassis), gJointType_FrontRight_c, aParticleList(cTruckStructure_ParticleFrontRightWheel).posCenterMass.X, aParticleList(cTruckStructure_ParticleFrontRightWheel).posCenterMass.Y + dampedSpringList(cDampedSpring_FrChassis).length * 100, cTruckStructure_DimChassisWidth / 2
    ' Compute the new position and rotation of center of mass of chassis
    Particle_SetCenterMassPositionAndRotation aParticleList(cTruckStructure_ParticleChassis)
    
End Sub

Public Sub Truck_UpdateWheelsRotationAngle(wheelsRotationAngle As Single)
    Dim cosVal As Double, sinVal As Double
    cosVal = Cos(wheelsRotationAngle)
    sinVal = Sin(wheelsRotationAngle)
    
    aParticleList(cTruckStructure_ParticleFrontLeftWheel).rotationAngle.Z = wheelsRotationAngle
    aParticleList(cTruckStructure_ParticleFrontLeftWheel).cosRotAngle.Z = cosVal
    aParticleList(cTruckStructure_ParticleFrontLeftWheel).sinRotAngle.Z = sinVal

    aParticleList(cTruckStructure_ParticleFrontRightWheel).rotationAngle.Z = wheelsRotationAngle
    aParticleList(cTruckStructure_ParticleFrontRightWheel).cosRotAngle.Z = cosVal
    aParticleList(cTruckStructure_ParticleFrontRightWheel).sinRotAngle.Z = sinVal

    aParticleList(cTruckStructure_ParticleRearLeftWheel).rotationAngle.Z = wheelsRotationAngle
    aParticleList(cTruckStructure_ParticleRearLeftWheel).cosRotAngle.Z = cosVal
    aParticleList(cTruckStructure_ParticleRearLeftWheel).sinRotAngle.Z = sinVal

    aParticleList(cTruckStructure_ParticleRearRightWheel).rotationAngle.Z = wheelsRotationAngle
    aParticleList(cTruckStructure_ParticleRearRightWheel).cosRotAngle.Z = cosVal
    aParticleList(cTruckStructure_ParticleRearRightWheel).sinRotAngle.Z = sinVal
End Sub

Public Sub Truck_UpdateWheelsSteeringAngle(wheelsSteeringAngle As Long)
    aParticleList(cTruckStructure_ParticleFrontLeftWheel).rotationAngle.Y = PI / 180 * CDbl(wheelsSteeringAngle)
    aParticleList(cTruckStructure_ParticleFrontRightWheel).rotationAngle.Y = PI / 180 * CDbl(wheelsSteeringAngle)
End Sub
