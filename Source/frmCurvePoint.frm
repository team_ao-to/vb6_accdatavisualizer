VERSION 5.00
Begin VB.Form frmCurvePoint 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Please enter curve point data"
   ClientHeight    =   1920
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   3855
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1920
   ScaleWidth      =   3855
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Height          =   1770
      Left            =   45
      TabIndex        =   4
      Top             =   45
      Width           =   3705
      Begin VB.CommandButton cmdOk 
         Caption         =   "Ok"
         Height          =   330
         Left            =   2430
         TabIndex        =   2
         Top             =   1260
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "Cancel"
         Height          =   330
         Left            =   1350
         TabIndex        =   3
         Top             =   1260
         Width           =   1005
      End
      Begin VB.TextBox txtY 
         Height          =   330
         Left            =   450
         TabIndex        =   1
         Text            =   "0"
         Top             =   630
         Width           =   1050
      End
      Begin VB.TextBox txtX 
         Height          =   330
         Left            =   450
         TabIndex        =   0
         Text            =   "0"
         Top             =   225
         Width           =   1050
      End
      Begin VB.Label Label1 
         Caption         =   "Y"
         Height          =   195
         Index           =   1
         Left            =   135
         TabIndex        =   6
         Top             =   675
         Width           =   375
      End
      Begin VB.Label Label1 
         Caption         =   "X"
         Height          =   195
         Index           =   0
         Left            =   135
         TabIndex        =   5
         Top             =   270
         Width           =   375
      End
   End
End
Attribute VB_Name = "frmCurvePoint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'-----------------------------------------------------------------------------------------------------------------------
'
' (c) Copyright 2016 by Ao-to Corporation. All rights reserved.
'
' The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
' No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
' the specific written permission signed by a legal representative of the Ao-to Corporation.
'
' Change log:
'
' ----------     -----------     -------------     ---------------------------------------------------------------------
' Date           Username        Ticket            Change description
' ----------     -----------     -------------     ---------------------------------------------------------------------
' 19.10.2016     razvan          AOTO2             Initial version of the file
'-----------------------------------------------------------------------------------------------------------------------

Private Sub Form_Load()
    Registry_GetFormPosition Me, False
    txtX = GetSetting(App.Title, "Settings", Me.Name & "txtX", 0)
    txtY = GetSetting(App.Title, "Settings", Me.Name & "txtY", 0)
    txtX.SelStart = 0
    txtX.SelLength = Len(txtX)
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Registry_SetFormPosition Me, False
End Sub

Private Sub cmdCancel_Click()
    SaveSetting App.Title, "Settings", Me.Name & "bOkPressed", "false"
    Unload Me
End Sub

Private Sub cmdOk_Click()
    SaveSetting App.Title, "Settings", Me.Name & "bOkPressed", "true"
    SaveSetting App.Title, "Settings", Me.Name & "txtX", txtX
    SaveSetting App.Title, "Settings", Me.Name & "txtY", txtY
    Unload Me
End Sub

Private Sub txtX_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case 13
            txtY.SetFocus
        Case vbKeyEscape
            Call cmdCancel_Click
    End Select
End Sub

Private Sub txtY_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case 13
            cmdOk.SetFocus
        Case vbKeyEscape
            Call cmdCancel_Click
    End Select
End Sub

Private Sub txtX_GotFocus()
    txtX.SelStart = 0
    txtX.SelLength = Len(txtX)
End Sub

Private Sub txtY_GotFocus()
    txtY.SelStart = 0
    txtY.SelLength = Len(txtY)
End Sub
