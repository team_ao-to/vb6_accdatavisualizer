Attribute VB_Name = "Module_Particle"
'-----------------------------------------------------------------------------------------------------------------------
'
' (c) Copyright 2016 by Ao-to Corporation. All rights reserved.
'
' The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
' No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
' the specific written permission signed by a legal representative of the Ao-to Corporation.
'
' Change log:
'
' ----------     -----------     -------------     ---------------------------------------------------------------------
' Date           Username        Ticket            Change description
' ----------     -----------     -------------     ---------------------------------------------------------------------
' 19.10.2016     razvan          AOTO2             Initial version of the file
'-----------------------------------------------------------------------------------------------------------------------

'Store some data types from draw module. If we moved them back to Draw module, VB reports a circular reference
Public Type drawPoint_t
    offset As vector_t
    projectedX As Double
    projectedY As Double
End Type

Public Type drawLine_t
    startPoint As Integer
    endPoint As Integer
    color As ColorConstants
End Type

' From here down we have the particle data types, constants, etc
Public Enum jointType_t
    gJointType_FrontLeft_c = 0
    gJointType_FrontRight_c = 1
    gJointType_Rear_c = 2
End Enum

Public Type particle_t
    posCenterMass As vector_t
    rotationAngle As vector_t
    velocity As vector_t
    acc As vector_t
    mass As Double
    
    sinRotAngle As vector_t
    cosRotAngle As vector_t
    
    ' Offsets of the front left, front right and rear joints
    flJointOffset As vector_t
    frJointOffset As vector_t
    rJointOffset As vector_t
    ' Positions of the front left, front right and rear joints
    flJointPos As vector_t
    frJointPos As vector_t
    rJointPos As vector_t
    
    ' Information related to drawing
    drawPoints() As drawPoint_t
    drawLines() As drawLine_t
End Type

' Declare all parts of the truck
Public aParticleList(cTruckStructure_ParticleMax) As particle_t


Public Sub Particle_Init(P As particle_t)
    Vector_Reset P.posCenterMass
    Vector_Reset P.rotationAngle
    Vector_Reset P.velocity
    Vector_Reset P.acc
    
    P.sinRotAngle.X = 0
    P.cosRotAngle.X = 1
    P.sinRotAngle.Y = 0
    P.cosRotAngle.Y = 1
    P.sinRotAngle.Z = 0
    P.cosRotAngle.Z = 1
    
    'Resize array of drawable lines and points to 0
    ReDim P.drawLines(0)
    ReDim P.drawPoints(0)
End Sub


Public Sub Particle_SetJointOffsets(P As particle_t, frontJointsWidth As Double, frontJointsOffsetX As Double, rearJointOffsetX As Double, allJointsYOffset As Double)
    ' Set offsets for front left joint
    P.flJointOffset.X = frontJointsOffsetX
    P.flJointOffset.Y = allJointsYOffset
    P.flJointOffset.Z = -frontJointsWidth / 2
    ' Set offsets for front right joint
    P.frJointOffset.X = frontJointsOffsetX
    P.frJointOffset.Y = allJointsYOffset
    P.frJointOffset.Z = frontJointsWidth / 2
    ' Set offsets for rear joint
    P.rJointOffset.X = rearJointOffsetX
    P.rJointOffset.Y = allJointsYOffset
    P.rJointOffset.Z = 0
End Sub

Public Sub Particle_SetJointPosition(P As particle_t, joint As jointType_t, X As Double, Y As Double, Z As Double)
    Select Case joint
        Case gJointType_FrontLeft_c
            P.flJointPos.X = X
            P.flJointPos.Y = Y
            P.flJointPos.Z = Z
        Case gJointType_FrontRight_c
            P.frJointPos.X = X
            P.frJointPos.Y = Y
            P.frJointPos.Z = Z
        Case gJointType_Rear_c
            P.rJointPos.X = X
            P.rJointPos.Y = Y
            P.rJointPos.Z = Z
    End Select
End Sub

'------------------------------------------------------------------------------------------------
' Particle related private functions
'------------------------------------------------------------------------------------------------

Public Sub Particle_SetCenterMassPositionAndRotation(P As particle_t)
    Dim frontJointsY As Double
    Dim jointsLengthX As Double
    Dim frontJointsWidth As Double

    frontJointsY = (P.flJointPos.Y + P.frJointPos.Y) / 2
    frontJointsWidth = P.frJointOffset.Z - P.flJointOffset.Z
    jointsLengthX = P.rJointOffset.X - P.frJointOffset.X
    
    ' Compute rotation angle on z axis
    P.rotationAngle.Z = Atn((P.rJointPos.Y - frontJointsY) / jointsLengthX)
    ' Compute rotation angle on x axis
    P.rotationAngle.X = Atn((P.flJointPos.Y - P.frJointPos.Y) / frontJointsWidth)
    
    'store cos and sin
    P.sinRotAngle.X = Sin(P.rotationAngle.X)
    P.sinRotAngle.Z = Sin(P.rotationAngle.Z)
    P.cosRotAngle.X = Cos(P.rotationAngle.X)
    P.cosRotAngle.Z = Cos(P.rotationAngle.Z)
    
    ' compute position of center of mass
    P.posCenterMass.X = P.flJointPos.X - P.flJointOffset.X * P.cosRotAngle.Z + P.flJointOffset.Y * P.sinRotAngle.Z
    P.posCenterMass.Y = P.flJointPos.Y - P.flJointOffset.Y * P.cosRotAngle.Z - P.flJointOffset.X * P.sinRotAngle.Z + P.flJointOffset.Z * P.sinRotAngle.X
    P.posCenterMass.Z = P.flJointPos.Z - P.flJointOffset.Z * P.cosRotAngle.X - P.flJointOffset.Y * P.sinRotAngle.X
End Sub

Sub Particle_UpdateCabinPosition()
    On Error Resume Next
    Particle_SetJointPosition aParticleList(cTruckStructure_ParticleCabin), gJointType_FrontLeft_c, dampedSpringList(cDampedSpring_FlCabin).basePos.pos.X, dampedSpringList(cDampedSpring_FlCabin).basePos.pos.Y + dampedSpringList(cDampedSpring_FlCabin).length / CSng(frmConfigurator.txtSpringMaxLength) * 0.6 * 100, dampedSpringList(cDampedSpring_FlCabin).basePos.pos.Z
    Particle_SetJointPosition aParticleList(cTruckStructure_ParticleCabin), gJointType_FrontRight_c, dampedSpringList(cDampedSpring_FrCabin).basePos.pos.X, dampedSpringList(cDampedSpring_FrCabin).basePos.pos.Y + dampedSpringList(cDampedSpring_FrCabin).length / CSng(frmConfigurator.txtSpringMaxLength) * 0.6 * 100, dampedSpringList(cDampedSpring_FrCabin).basePos.pos.Z
    Particle_SetJointPosition aParticleList(cTruckStructure_ParticleCabin), gJointType_Rear_c, dampedSpringList(cDampedSpring_RCabin).basePos.pos.X, dampedSpringList(cDampedSpring_RCabin).basePos.pos.Y + dampedSpringList(cDampedSpring_RCabin).length / CSng(frmConfigurator.txtSpringMaxLength) * 0.6 * 100, dampedSpringList(cDampedSpring_RCabin).basePos.pos.Z
    Particle_SetCenterMassPositionAndRotation aParticleList(cTruckStructure_ParticleCabin)
End Sub

