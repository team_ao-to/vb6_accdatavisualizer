Attribute VB_Name = "Module_Configuration"
'-----------------------------------------------------------------------------------------------------------------------
'
' (c) Copyright 2016 by Ao-to Corporation. All rights reserved.
'
' The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
' No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
' the specific written permission signed by a legal representative of the Ao-to Corporation.
'
' Change log:
'
' ----------     -----------     -------------     ---------------------------------------------------------------------
' Date           Username        Ticket            Change description
' ----------     -----------     -------------     ---------------------------------------------------------------------
' 19.10.2016     razvan          AOTO2             Initial version of the file
'-----------------------------------------------------------------------------------------------------------------------

' Truck module data that can be configured in order to modify the behaviour of the movement
Public Const cTruck_CabinMass = 1600 'Kg

' DampedSpring module data that can be configured in order to modify the behaviour of the string
Public Const cDampedSpring_RestLength = 0.6 'meters
Public Const cDampedSpring_MinLength = 0  'meters
Public Const cDampedSpring_MaxLength = 0.6 'meters


