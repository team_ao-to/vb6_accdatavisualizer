Attribute VB_Name = "Module_Scope"
'-----------------------------------------------------------------------------------------------------------------------
'
' (c) Copyright 2016 by Ao-to Corporation. All rights reserved.
'
' The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
' No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
' the specific written permission signed by a legal representative of the Ao-to Corporation.
'
' Change log:
'
' ----------     -----------     -------------     ---------------------------------------------------------------------
' Date           Username        Ticket            Change description
' ----------     -----------     -------------     ---------------------------------------------------------------------
' 19.10.2016     razvan          AOTO2             Initial version of the file
'-----------------------------------------------------------------------------------------------------------------------

'Max num samples of data in the buffers of the scope. 30000 coresponds to 300 sec = 5 mins
Public Const cScopeMaxNumSamples = 30000

'Accelerometer values are expressed in 0.06 mG. The max value of acceleration measured on Z is let's say 1.5G
Public Const cScopeMaxDisplayedValue = 25000
Public Const cScopeMinDisplayedValue = -25000

Public scopeCircularBufferAccDataRaw(cAcc_NumAxes, cScopeMaxNumSamples) As Long
Public scopeCircularBufferAccDataFiltered(cAcc_NumAxes, cScopeMaxNumSamples) As Long
Public scopeCircularBufferSpringLength(cDampedSpring_NumCabinSprings, cScopeMaxNumSamples) As Long
Public scopeCircularBufferTruckSpeedPercent(cScopeMaxNumSamples) As Long
Public scopeCircularBufferTruckWheelsRotationAngle(cScopeMaxNumSamples) As Single
Public scopeCircularBufferTruckWheelsSteeringAngle(cScopeMaxNumSamples) As Long

Public scopeCircularBufferHead As Long
Public scopeCircularBufferTail As Long
Public scopeNumDataPointsInCircularBuffers As Long

Public scopeMarkerVal(2) As Long
Public bScopeMarkerVisible(2) As Boolean
Public scopeLastMarker As Integer

Public scopeCurrentPlayedOrRecordedDataSample As Long

Public Sub Scope_Init()
    scopeCircularBufferHead = 0
    scopeCircularBufferTail = 0
    scopeNumDataPointsInCircularBuffers = 0
End Sub

Public Sub Scope_PushDataInCircularBuffers(rawAccX As Long, rawAccY As Long, rawAccZ As Long, filteredAccX As Long, filteredAccY As Long, filteredAccZ As Long, springLenghtFL As Long, springLenghtFR As Long, springLenghtR As Long, truckSpeedPercent As Long, wheelsRotationAngle As Single, wheelsSteeringAngle As Long)
    
    scopeCircularBufferAccDataRaw(0, scopeCircularBufferHead) = rawAccX
    scopeCircularBufferAccDataRaw(1, scopeCircularBufferHead) = rawAccY
    scopeCircularBufferAccDataRaw(2, scopeCircularBufferHead) = rawAccZ
    
    scopeCircularBufferAccDataFiltered(0, scopeCircularBufferHead) = filteredAccX
    scopeCircularBufferAccDataFiltered(1, scopeCircularBufferHead) = filteredAccY
    scopeCircularBufferAccDataFiltered(2, scopeCircularBufferHead) = filteredAccZ

    scopeCircularBufferSpringLength(cDampedSpring_FlCabin, scopeCircularBufferHead) = springLenghtFL
    scopeCircularBufferSpringLength(cDampedSpring_FrCabin, scopeCircularBufferHead) = springLenghtFR
    scopeCircularBufferSpringLength(cDampedSpring_RCabin, scopeCircularBufferHead) = springLenghtR
    
    scopeCircularBufferTruckSpeedPercent(scopeCircularBufferHead) = truckSpeedPercent
    scopeCircularBufferTruckWheelsRotationAngle(scopeCircularBufferHead) = wheelsRotationAngle
    scopeCircularBufferTruckWheelsSteeringAngle(scopeCircularBufferHead) = wheelsSteeringAngle
    
    scopeCurrentPlayedOrRecordedDataSample = scopeCircularBufferHead

    scopeCircularBufferHead = Scope_CircularAdd(scopeCircularBufferHead, 1)
    
    ' If the circular buffer is already full, drop 1 sample from the tail (oldest data)
    If scopeCircularBufferHead = scopeCircularBufferTail Then
        scopeCircularBufferTail = Scope_CircularAdd(scopeCircularBufferTail, 1)
    Else
        scopeNumDataPointsInCircularBuffers = scopeNumDataPointsInCircularBuffers + 1
        'Update HScroll
        frmScopeView.ScopeView_UpdateHScrollMaxValue
    End If
End Sub

Public Function Scope_GetTailPosition(offsetFromTail As Long) As Long
    Dim pos As Long
    pos = scopeCircularBufferTail + offsetFromTail
    If (pos >= cScopeMaxNumSamples) Then
        pos = pos - cScopeMaxNumSamples
    End If
    Scope_GetTailPosition = pos
End Function

Public Function Scope_CircularAdd(idxDataStart As Long, addedVal As Long) As Long
    Dim retVal As Long
    retVal = idxDataStart + addedVal
    If retVal >= cScopeMaxNumSamples Then
        retVal = retVal - cScopeMaxNumSamples
    End If
    Scope_CircularAdd = retVal
End Function

Public Function Scope_CircularSubstract(idxDataStart As Long, substractedVal As Long) As Long
    Dim retVal As Long
    retVal = idxDataStart - substractedVal
    If retVal < 0 Then
        retVal = cScopeMaxNumSamples + retVal
    End If
    Scope_CircularSubstract = retVal
End Function

Public Function Scope_GetDataSampleDistanceFromTail(idxDataSample As Long)
    Scope_GetDataSampleDistanceFromTail = Scope_CircularSubstract(idxDataSample, scopeCircularBufferTail)
End Function

Public Function Scope_GetDataSampleDistanceFromHead(idxDataSample As Long)
    Scope_GetDataSampleDistanceFromHead = Scope_CircularSubstract(scopeCircularBufferHead, idxDataSample)
End Function

