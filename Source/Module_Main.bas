Attribute VB_Name = "Module_Main"
'-----------------------------------------------------------------------------------------------------------------------
'
' (c) Copyright 2016 by Ao-to Corporation. All rights reserved.
'
' The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
' No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
' the specific written permission signed by a legal representative of the Ao-to Corporation.
'
' Change log:
'
' ----------     -----------     -------------     ---------------------------------------------------------------------
' Date           Username        Ticket            Change description
' ----------     -----------     -------------     ---------------------------------------------------------------------
' 19.10.2016     razvan          AOTO2             Initial version of the file
'-----------------------------------------------------------------------------------------------------------------------

'This is the first function called when application starts
Sub Main()
    ' Do some initialization
    ' Initialize camera
    Camera_Init
    ' Initialize scope
    Scope_Init
    ' MARKER - Change to other filter init function, if another filter is wanted
    AccFilter2_Init
    ' Initialize damped springs
    DampedSpring_Init
    ' Initialize the truck structure, composed from a number of particles
    Truck_Init
    ' Now that the truck components are in place, update the base positions of the cabin springs
    DampedSpring_UpdateCabinSpringsBasePositions
    ' Now that the cabin springs base positions are updated, update the position of the cabin in space
    Particle_UpdateCabinPosition
    ' Initialize module computing math curves
    Curve_Init
    ' Show the forms
    frmConfigurator.Show
    frmTruck3DView.Show
    'frmPlayMovie.Show
    frmScopeView.Show
End Sub

Public Sub Main_CloseAllForms()
    Unload frmTruck3DView
    Unload frmScopeView
    Unload frmConfigurator
    Unload frmPlayMovie
End Sub
