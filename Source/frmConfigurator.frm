VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form frmConfigurator 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Configurator"
   ClientHeight    =   5220
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   10785
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5220
   ScaleWidth      =   10785
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer tmrStatus 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   3690
      Top             =   4770
   End
   Begin VB.CommandButton cmdSetDataToTruck 
      Caption         =   "Set data to truck"
      Height          =   285
      Left            =   8865
      TabIndex        =   8
      Top             =   4860
      Width           =   1860
   End
   Begin VB.CommandButton cmdGetDataFromTruck 
      Caption         =   "Get data from truck"
      Height          =   285
      Left            =   6705
      TabIndex        =   7
      Top             =   4860
      Width           =   1860
   End
   Begin TabDlg.SSTab tabControl 
      Height          =   4695
      Left            =   45
      TabIndex        =   3
      Top             =   90
      Width           =   10680
      _ExtentX        =   18838
      _ExtentY        =   8281
      _Version        =   393216
      Style           =   1
      Tabs            =   6
      TabsPerRow      =   6
      TabHeight       =   520
      TabCaption(0)   =   "Misc"
      TabPicture(0)   =   "frmConfigurator.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblSpringRestLength"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblSpringMaxLength"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblCabinMass"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "txtSpringRestLength"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "txtSpringMaxLength"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "txtCabinMass"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).ControlCount=   6
      TabCaption(1)   =   "Spring stifness"
      TabPicture(1)   =   "frmConfigurator.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "chart(0)"
      Tab(1).Control(1)=   "cmdRemove(0)"
      Tab(1).Control(2)=   "cmdAdd(0)"
      Tab(1).Control(3)=   "listView(0)"
      Tab(1).ControlCount=   4
      TabCaption(2)   =   "Spring damping"
      TabPicture(2)   =   "frmConfigurator.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "cmdAdd(1)"
      Tab(2).Control(1)=   "cmdRemove(1)"
      Tab(2).Control(2)=   "listView(1)"
      Tab(2).Control(3)=   "chart(1)"
      Tab(2).ControlCount=   4
      TabCaption(3)   =   "Acc front-rear"
      TabPicture(3)   =   "frmConfigurator.frx":0054
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "cmdAdd(2)"
      Tab(3).Control(1)=   "cmdRemove(2)"
      Tab(3).Control(2)=   "listView(2)"
      Tab(3).Control(3)=   "chart(2)"
      Tab(3).ControlCount=   4
      TabCaption(4)   =   "Acc left-right"
      TabPicture(4)   =   "frmConfigurator.frx":0070
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "cmdAdd(3)"
      Tab(4).Control(1)=   "cmdRemove(3)"
      Tab(4).Control(2)=   "listView(3)"
      Tab(4).Control(3)=   "chart(3)"
      Tab(4).ControlCount=   4
      TabCaption(5)   =   "Acc up-down"
      TabPicture(5)   =   "frmConfigurator.frx":008C
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "chart(4)"
      Tab(5).Control(1)=   "listView(4)"
      Tab(5).Control(2)=   "cmdRemove(4)"
      Tab(5).Control(3)=   "cmdAdd(4)"
      Tab(5).ControlCount=   4
      Begin MSChart20Lib.MSChart chart 
         Height          =   4020
         Index           =   0
         Left            =   -71265
         OleObjectBlob   =   "frmConfigurator.frx":00A8
         TabIndex        =   25
         Top             =   495
         Width           =   6720
      End
      Begin VB.CommandButton cmdAdd 
         Caption         =   "+"
         Height          =   330
         Index           =   4
         Left            =   -71715
         TabIndex        =   12
         Top             =   495
         Width           =   375
      End
      Begin VB.CommandButton cmdRemove 
         Caption         =   "-"
         Height          =   330
         Index           =   4
         Left            =   -71715
         TabIndex        =   16
         Top             =   945
         Width           =   375
      End
      Begin VB.CommandButton cmdAdd 
         Caption         =   "+"
         Height          =   330
         Index           =   3
         Left            =   -71760
         TabIndex        =   22
         Top             =   495
         Width           =   375
      End
      Begin VB.CommandButton cmdRemove 
         Caption         =   "-"
         Height          =   330
         Index           =   3
         Left            =   -71760
         TabIndex        =   21
         Top             =   945
         Width           =   375
      End
      Begin VB.CommandButton cmdAdd 
         Caption         =   "+"
         Height          =   330
         Index           =   2
         Left            =   -71760
         TabIndex        =   18
         Top             =   495
         Width           =   375
      End
      Begin VB.CommandButton cmdRemove 
         Caption         =   "-"
         Height          =   330
         Index           =   2
         Left            =   -71760
         TabIndex        =   17
         Top             =   945
         Width           =   375
      End
      Begin VB.CommandButton cmdAdd 
         Caption         =   "+"
         Height          =   330
         Index           =   1
         Left            =   -71760
         TabIndex        =   14
         Top             =   405
         Width           =   375
      End
      Begin VB.CommandButton cmdRemove 
         Caption         =   "-"
         Height          =   330
         Index           =   1
         Left            =   -71760
         TabIndex        =   13
         Top             =   855
         Width           =   375
      End
      Begin VB.CommandButton cmdRemove 
         Caption         =   "-"
         Height          =   330
         Index           =   0
         Left            =   -71760
         TabIndex        =   11
         Top             =   900
         Width           =   375
      End
      Begin VB.CommandButton cmdAdd 
         Caption         =   "+"
         Height          =   330
         Index           =   0
         Left            =   -71760
         TabIndex        =   10
         Top             =   450
         Width           =   375
      End
      Begin MSComctlLib.ListView listView 
         Height          =   4065
         Index           =   0
         Left            =   -74910
         TabIndex        =   9
         Top             =   450
         Width           =   2985
         _ExtentX        =   5265
         _ExtentY        =   7170
         SortKey         =   3
         View            =   3
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Pos"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   1
            Text            =   "X"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   2
            Text            =   "Y"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "X Sort"
            Object.Width           =   0
         EndProperty
      End
      Begin VB.TextBox txtCabinMass 
         Height          =   285
         Left            =   1935
         TabIndex        =   2
         Top             =   1305
         Width           =   780
      End
      Begin VB.TextBox txtSpringMaxLength 
         Height          =   285
         Left            =   1935
         TabIndex        =   1
         Top             =   900
         Width           =   780
      End
      Begin VB.TextBox txtSpringRestLength 
         Height          =   285
         Left            =   1935
         TabIndex        =   0
         Top             =   495
         Width           =   780
      End
      Begin MSComctlLib.ListView listView 
         Height          =   4065
         Index           =   1
         Left            =   -74910
         TabIndex        =   15
         Top             =   405
         Width           =   2985
         _ExtentX        =   5265
         _ExtentY        =   7170
         SortKey         =   3
         View            =   3
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Pos"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   1
            Text            =   "X"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   2
            Text            =   "Y"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "X Sort"
            Object.Width           =   0
         EndProperty
      End
      Begin MSComctlLib.ListView listView 
         Height          =   4065
         Index           =   2
         Left            =   -74910
         TabIndex        =   19
         Top             =   495
         Width           =   2985
         _ExtentX        =   5265
         _ExtentY        =   7170
         SortKey         =   3
         View            =   3
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Pos"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   1
            Text            =   "X"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   2
            Text            =   "Y"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "X Sort"
            Object.Width           =   0
         EndProperty
      End
      Begin MSComctlLib.ListView listView 
         Height          =   4065
         Index           =   3
         Left            =   -74910
         TabIndex        =   23
         Top             =   495
         Width           =   2985
         _ExtentX        =   5265
         _ExtentY        =   7170
         SortKey         =   3
         View            =   3
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Pos"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   1
            Text            =   "X"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   2
            Text            =   "Y"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "X Sort"
            Object.Width           =   0
         EndProperty
      End
      Begin MSComctlLib.ListView listView 
         Height          =   4065
         Index           =   4
         Left            =   -74865
         TabIndex        =   20
         Top             =   495
         Width           =   2985
         _ExtentX        =   5265
         _ExtentY        =   7170
         SortKey         =   3
         View            =   3
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Pos"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   1
            Text            =   "X"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   2
            Text            =   "Y"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "X Sort"
            Object.Width           =   0
         EndProperty
      End
      Begin MSChart20Lib.MSChart chart 
         Height          =   4020
         Index           =   1
         Left            =   -71265
         OleObjectBlob   =   "frmConfigurator.frx":23FE
         TabIndex        =   26
         Top             =   405
         Width           =   6720
      End
      Begin MSChart20Lib.MSChart chart 
         Height          =   4020
         Index           =   2
         Left            =   -71220
         OleObjectBlob   =   "frmConfigurator.frx":4754
         TabIndex        =   27
         Top             =   495
         Width           =   6720
      End
      Begin MSChart20Lib.MSChart chart 
         Height          =   4020
         Index           =   3
         Left            =   -71220
         OleObjectBlob   =   "frmConfigurator.frx":6AAA
         TabIndex        =   28
         Top             =   495
         Width           =   6720
      End
      Begin MSChart20Lib.MSChart chart 
         Height          =   4020
         Index           =   4
         Left            =   -71220
         OleObjectBlob   =   "frmConfigurator.frx":8E00
         TabIndex        =   24
         Top             =   450
         Width           =   6720
      End
      Begin VB.Label lblCabinMass 
         AutoSize        =   -1  'True
         Caption         =   "Cabin mass (kg):"
         Height          =   195
         Left            =   315
         TabIndex        =   6
         Top             =   1305
         Width           =   1170
      End
      Begin VB.Label lblSpringMaxLength 
         AutoSize        =   -1  'True
         Caption         =   "Spring max length (m):"
         Height          =   195
         Left            =   315
         TabIndex        =   5
         Top             =   922
         Width           =   1560
      End
      Begin VB.Label lblSpringRestLength 
         AutoSize        =   -1  'True
         Caption         =   "Spring rest length (m):"
         Height          =   195
         Left            =   315
         TabIndex        =   4
         Top             =   540
         Width           =   1530
      End
   End
   Begin VB.Label lblStatus 
      Height          =   195
      Left            =   90
      TabIndex        =   29
      Top             =   4905
      Width           =   3345
   End
End
Attribute VB_Name = "frmConfigurator"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'-----------------------------------------------------------------------------------------------------------------------
'
' (c) Copyright 2016 by Ao-to Corporation. All rights reserved.
'
' The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
' No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
' the specific written permission signed by a legal representative of the Ao-to Corporation.
'
' Change log:
'
' ----------     -----------     -------------     ---------------------------------------------------------------------
' Date           Username        Ticket            Change description
' ----------     -----------     -------------     ---------------------------------------------------------------------
' 19.10.2016     razvan          AOTO2             Initial version of the file
'-----------------------------------------------------------------------------------------------------------------------

Private Enum tab_e
    eTab_SpringStifness = 0
    eTab_SpringDamping = 1
    eTab_AccFrontRear = 2
    eTab_AccLeftRight = 3
    eTab_AccUpDown = 4
End Enum





Private Sub cmdSetDataToTruck_Click()
    Dim msg(435) As Byte
    ' Add header and msgType
    msg(0) = 255
    msg(1) = 250
    msg(2) = eSerialMsgType_SetAllParamsReq
    
    ' Insert springRestLength, maxLenght and cabinMass
    ByteConverter_SetSingle msg, 3, CSng(txtSpringRestLength.Text)
    ByteConverter_SetSingle msg, 7, CSng(txtSpringMaxLength.Text)
    ByteConverter_SetSingle msg, 11, CSng(txtCabinMass.Text)
    ' Insert curves
    Configurator_InsertCurveDataInSerialMsg msg, 15, eTab_SpringStifness
    Configurator_InsertCurveDataInSerialMsg msg, 99, eTab_SpringDamping
    Configurator_InsertCurveDataInSerialMsg msg, 183, eTab_AccFrontRear
    Configurator_InsertCurveDataInSerialMsg msg, 267, eTab_AccLeftRight
    Configurator_InsertCurveDataInSerialMsg msg, 351, eTab_AccUpDown

    
    TabMisc_StoreInRegistry
    Tab_StoreInRegistry eTab_SpringStifness
    Tab_StoreInRegistry eTab_SpringDamping
    Tab_StoreInRegistry eTab_AccFrontRear
    Tab_StoreInRegistry eTab_AccLeftRight
    Tab_StoreInRegistry eTab_AccUpDown
    
    frmScopeView.comPort.Output = msg
End Sub

Private Sub Configurator_InsertCurveDataInSerialMsg(b() As Byte, start As Integer, idx As tab_e)
    Dim numCurvePoints As Integer, i As Integer
    Dim idxByteToWrite As Integer
    
    numCurvePoints = listView(idx).ListItems.Count
    If numCurvePoints > cCurve_MaxNumPointsInCurve Then
        numCurvePoints = cCurve_MaxNumPointsInCurve
    End If
    idxByteToWrite = start
    'Insert first curve length
    b(idxByteToWrite) = numCurvePoints
    idxByteToWrite = idxByteToWrite + 4
    'Insert curveData
    For i = 1 To numCurvePoints
       ByteConverter_SetSingle b, idxByteToWrite, CSng(listView(idx).ListItems.Item(i).SubItems(1))
       idxByteToWrite = idxByteToWrite + 4
       ByteConverter_SetSingle b, idxByteToWrite, CSng(listView(idx).ListItems.Item(i).SubItems(2))
       idxByteToWrite = idxByteToWrite + 4
    Next
End Sub



Private Sub Form_Load()
    Registry_GetFormPosition Me, False
    'Bring values from registry for tab MISC
    TabMisc_RestoreFromRegistry
    'Bring values from registry for the other tabs
    Tab_RestoreFromRegistry eTab_SpringStifness
    Tab_RestoreFromRegistry eTab_SpringDamping
    Tab_RestoreFromRegistry eTab_AccFrontRear
    Tab_RestoreFromRegistry eTab_AccLeftRight
    Tab_RestoreFromRegistry eTab_AccUpDown
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Registry_SetFormPosition Me, False
  
    Main_CloseAllForms
End Sub

'---------------------------------------------------------------------------------------------------------------------
' TAB MISC related functions
'---------------------------------------------------------------------------------------------------------------------
Private Sub TabMisc_RestoreFromRegistry()
    txtSpringRestLength.Text = GetSetting(App.Title, "Settings", Me.Name & "txtSpringRestLength", cDampedSpring_RestLength)
    txtSpringMaxLength.Text = GetSetting(App.Title, "Settings", Me.Name & "txtSpringMaxLength", cDampedSpring_MaxLength)
    txtCabinMass.Text = GetSetting(App.Title, "Settings", Me.Name & "txtCabinMass", cTruck_CabinMass)
End Sub

Private Sub TabMisc_StoreInRegistry()
    SaveSetting App.Title, "Settings", Me.Name & "txtSpringRestLength", txtSpringRestLength.Text
    SaveSetting App.Title, "Settings", Me.Name & "txtSpringMaxLength", txtSpringMaxLength.Text
    SaveSetting App.Title, "Settings", Me.Name & "txtCabinMass", txtCabinMass.Text
End Sub

Private Sub tmrStatus_Timer()
    lblStatus.Visible = False
    tmrStatus.Enabled = False
End Sub

' Got focus events on txtBoxes
Private Sub txtSpringRestLength_GotFocus()
    txtSpringRestLength.SelStart = 0
    txtSpringRestLength.SelLength = Len(txtSpringRestLength)
End Sub

Private Sub txtSpringMaxLength_GotFocus()
    txtSpringMaxLength.SelStart = 0
    txtSpringMaxLength.SelLength = Len(txtSpringMaxLength)
End Sub

Private Sub txtCabinMass_GotFocus()
    txtCabinMass.SelStart = 0
    txtCabinMass.SelLength = Len(txtCabinMass)
End Sub


'---------------------------------------------------------------------------------------------------------------------
' TABs with charts related functions
'---------------------------------------------------------------------------------------------------------------------

' Generic MsChart functions
Private Sub Chart_FillWithDataFromList(idx As Integer) 'chart As MSChart, list As listView)
    Dim i As Integer
    Dim displayedData() As Variant
    
    If listView(idx).ListItems.Count < 2 Then
        chart(idx).Visible = False
    Else
        ReDim displayedData(1 To listView(idx).ListItems.Count, 1 To 2)
    
        For i = 1 To listView(idx).ListItems.Count
            displayedData(i, 1) = " " & CStr(listView(idx).ListItems.Item(i).SubItems(1))
            displayedData(i, 2) = listView(idx).ListItems.Item(i).SubItems(2)
        Next
    
        With chart(idx)
            .chartType = VtChChartType2dLine
            .Plot.axis(VtChAxisIdX).CategoryScale.LabelTick = True
            .RandomFill = False
            .ChartData = displayedData
            .Visible = True
        End With
    End If
End Sub

Private Function Chart_InvertNumber(ByVal Number As String) As String
    Static i As Integer
    For i = 1 To Len(Number)
        Select Case Mid$(Number, i, 1)
        Case "-": Mid$(Number, i, 1) = " "
        Case "0": Mid$(Number, i, 1) = "9"
        Case "1": Mid$(Number, i, 1) = "8"
        Case "2": Mid$(Number, i, 1) = "7"
        Case "3": Mid$(Number, i, 1) = "6"
        Case "4": Mid$(Number, i, 1) = "5"
        Case "5": Mid$(Number, i, 1) = "4"
        Case "6": Mid$(Number, i, 1) = "3"
        Case "7": Mid$(Number, i, 1) = "2"
        Case "8": Mid$(Number, i, 1) = "1"
        Case "9": Mid$(Number, i, 1) = "0"
        End Select
    Next
    Chart_InvertNumber = Number
End Function


' Generic ListView functions
Private Sub ListView_RenumberItems(idx As Integer)
    Dim i As Integer
    For i = 1 To listView(idx).ListItems.Count
        listView(idx).ListItems(i).Text = CStr(i)
    Next
End Sub

' Specific buttons
Private Sub cmdRemove_Click(Index As Integer)
    listView(Index).ListItems.Remove listView(Index).SelectedItem.Index
    ListView_RenumberItems Index
End Sub

Private Sub cmdAdd_Click(Index As Integer)
    Dim lstItem As ListItem
    Dim txtX As String
    frmCurvePoint.Show 1
    If GetSetting(App.Title, "Settings", "frmCurvePoint" & "bOkPressed") = "false" Then
        Exit Sub
    End If
    
    Set lstItem = listView(Index).ListItems.Add(, , listView(Index).ListItems.Count + 1)
    txtX = GetSetting(App.Title, "Settings", "frmCurvePoint" & "txtX")
    lstItem.SubItems(1) = txtX
    lstItem.SubItems(2) = GetSetting(App.Title, "Settings", "frmCurvePoint" & "txtY")
    If CDbl(txtX) >= 0 Then
        lstItem.SubItems(3) = Format(txtX, "000000000.00")
    Else
        lstItem.SubItems(3) = "&" & Chart_InvertNumber(Format(0 - CDbl(txtX), "000000000.00"))
    End If
    ListView_RenumberItems Index
    Chart_FillWithDataFromList Index
End Sub

Private Sub Tab_RestoreFromRegistry(idx As Integer)
    Dim numItems As Integer
    Dim lstItem As ListItem
    
    numItems = GetSetting(App.Title, "Settings", "frmConfiguratorTab" & CStr(idx) & "Count", 0)
    For i = 1 To numItems
        Set lstItem = listView(idx).ListItems.Add(, , listView(idx).ListItems.Count + 1)
        lstItem.SubItems(1) = GetSetting(App.Title, "Settings", "frmConfiguratorTab" & CStr(idx) & "Item" & CStr(i) & "Subitem1", "")
        lstItem.SubItems(2) = GetSetting(App.Title, "Settings", "frmConfiguratorTab" & CStr(idx) & "Item" & CStr(i) & "Subitem2", "")
        lstItem.SubItems(3) = GetSetting(App.Title, "Settings", "frmConfiguratorTab" & CStr(idx) & "Item" & CStr(i) & "Subitem3", "")
    Next
    Chart_FillWithDataFromList idx
End Sub

Private Sub Tab_StoreInRegistry(idx As Integer)
    SaveSetting App.Title, "Settings", "frmConfiguratorTab" & CStr(idx) & "Count", listView(idx).ListItems.Count
    For i = 1 To listView(idx).ListItems.Count
        SaveSetting App.Title, "Settings", "frmConfiguratorTab" & CStr(idx) & "Item" & CStr(i) & "Subitem1", CStr(listView(idx).ListItems.Item(i).SubItems(1))
        SaveSetting App.Title, "Settings", "frmConfiguratorTab" & CStr(idx) & "Item" & CStr(i) & "Subitem2", CStr(listView(idx).ListItems.Item(i).SubItems(2))
        SaveSetting App.Title, "Settings", "frmConfiguratorTab" & CStr(idx) & "Item" & CStr(i) & "Subitem3", CStr(listView(idx).ListItems.Item(i).SubItems(3))
    Next
End Sub

