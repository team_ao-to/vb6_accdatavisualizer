Attribute VB_Name = "Module_Registry"
'-----------------------------------------------------------------------------------------------------------------------
'
' (c) Copyright 2016 by Ao-to Corporation. All rights reserved.
'
' The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
' No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
' the specific written permission signed by a legal representative of the Ao-to Corporation.
'
' Change log:
'
' ----------     -----------     -------------     ---------------------------------------------------------------------
' Date           Username        Ticket            Change description
' ----------     -----------     -------------     ---------------------------------------------------------------------
' 19.10.2016     razvan          AOTO2             Initial version of the file
'-----------------------------------------------------------------------------------------------------------------------

Public Sub Registry_GetFormPosition(frm As Form, Optional WidthHeight As Boolean = True)
    On Error Resume Next
    frm.Left = GetSetting(App.Title, "Settings", frm.Name & "left", (Screen.width - frm.width) / 2)
    frm.Top = GetSetting(App.Title, "Settings", frm.Name & "top", (Screen.height - frm.height) / 2)
    If WidthHeight = True Then
        frm.width = GetSetting(App.Title, "Settings", frm.Name & "width")
        frm.height = GetSetting(App.Title, "Settings", frm.Name & "height")
    End If
End Sub

Public Sub Registry_SetFormPosition(frm As Form, Optional WidthHeight As Boolean = True)
    On Error Resume Next
    SaveSetting App.Title, "Settings", frm.Name & "left", frm.Left
    SaveSetting App.Title, "Settings", frm.Name & "top", frm.Top
    If WidthHeight = True Then
        SaveSetting App.Title, "Settings", frm.Name & "width", frm.width
        SaveSetting App.Title, "Settings", frm.Name & "height", frm.height
    End If
End Sub

