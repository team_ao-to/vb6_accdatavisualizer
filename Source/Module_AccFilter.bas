Attribute VB_Name = "Module_AccFilter"
'-----------------------------------------------------------------------------------------------------------------------
'
' (c) Copyright 2016 by Ao-to Corporation. All rights reserved.
'
' The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
' No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
' the specific written permission signed by a legal representative of the Ao-to Corporation.
'
' Change log:
'
' ----------     -----------     -------------     ---------------------------------------------------------------------
' Date           Username        Ticket            Change description
' ----------     -----------     -------------     ---------------------------------------------------------------------
' 19.10.2016     razvan          AOTO2             Initial version of the file
'-----------------------------------------------------------------------------------------------------------------------

'Section for const and variables of AccFilter 1
'--------------------------------------------------------------------------------------------
Private Const cAccFilter1_MaxSamplesInFilter = 20
Private accFilter1_Buffer(3, cAccFilter1_MaxSamplesInFilter) As Long
Private accFilter1_NumReceivedSamples(3) As Long






'Section for const and variables of AccFilter 2
'--------------------------------------------------------------------------------------------

Private Const cAccFilter2_MaxSamplesInFilter = 41
Private accFilter2_FIR_Coeffs(cAccFilter2_MaxSamplesInFilter)
Private accFilter2_Buffer(3, cAccFilter2_MaxSamplesInFilter) As Double
Private accFilter2_NumReceivedSamples(3) As Long

'Please add code here






'Section for functions of AccFilter 1
'--------------------------------------------------------------------------------------------
Public Function AccFilter1_Init()
    Dim i As Integer
    ' Clear buffer used for filtering
    For i = 0 To cAccFilter1_MaxSamplesInFilter - 1
        accFilter1_Buffer(0, i) = 0
        accFilter1_Buffer(1, i) = 0
        accFilter1_Buffer(2, i) = 0
    Next
    accFilter1_NumReceivedSamples(0) = 0
    accFilter1_NumReceivedSamples(1) = 0
    accFilter1_NumReceivedSamples(2) = 0
End Function

Public Function AccFilter1_FilterData(axis As Integer, rawValue As Long) As Long
    Dim filterSum As Long
    Dim i As Integer
    
    filterSum = 0
    'Increase number of received data so far
    accFilter1_NumReceivedSamples(axis) = accFilter1_NumReceivedSamples(axis) + 1
    
    For i = 0 To cAccFilter1_MaxSamplesInFilter - 2
        accFilter1_Buffer(axis, i) = accFilter1_Buffer(axis, i + 1)
        filterSum = filterSum + accFilter1_Buffer(axis, i)
    Next
    accFilter1_Buffer(axis, cAccFilter1_MaxSamplesInFilter - 1) = rawValue
    'If we did not receive at least the size of the filter buffer, return the rawData
    If accFilter1_NumReceivedSamples(axis) < cAccFilter1_MaxSamplesInFilter Then
        AccFilter1_FilterData = rawValue
    Else
        filterSum = filterSum + rawValue
        AccFilter1_FilterData = CLng(CDbl(filterSum) / CDbl(cAccFilter1_MaxSamplesInFilter))
    End If
End Function


'Section for functions of AccFilter 2
'--------------------------------------------------------------------------------------------
'--------------------------------------------------------------------------------------------
Public Function AccFilter2_Init()
    Dim i As Integer
    ' Clear buffer used for filtering
    For i = 0 To cAccFilter2_MaxSamplesInFilter - 1
        accFilter2_Buffer(0, i) = 0
        accFilter2_Buffer(1, i) = 0
        accFilter2_Buffer(2, i) = 0
    Next
    accFilter2_NumReceivedSamples(0) = 0
    accFilter2_NumReceivedSamples(1) = 0
    accFilter2_NumReceivedSamples(2) = 0
    'VB6 is the best :)
    accFilter2_FIR_Coeffs(0) = -5.60304959751038E-03
    accFilter2_FIR_Coeffs(1) = -1.04117988138027E-02
    accFilter2_FIR_Coeffs(2) = -8.64767900257288E-03
    accFilter2_FIR_Coeffs(3) = -9.88942220373758E-03
    accFilter2_FIR_Coeffs(4) = -3.60483940737825E-03
    accFilter2_FIR_Coeffs(5) = 2.7539533211933E-03
    accFilter2_FIR_Coeffs(6) = 1.16405615281411E-02
    accFilter2_FIR_Coeffs(7) = 1.67302399377794E-02
    accFilter2_FIR_Coeffs(8) = 0.017069344965177
    accFilter2_FIR_Coeffs(9) = 9.47028825917822E-03
    accFilter2_FIR_Coeffs(10) = -4.23893581240706E-03
    accFilter2_FIR_Coeffs(11) = -0.020976137779659
    accFilter2_FIR_Coeffs(12) = -3.41726904831107E-02
    accFilter2_FIR_Coeffs(13) = -3.73280854708821E-02
    accFilter2_FIR_Coeffs(14) = -0.02486029788035
    accFilter2_FIR_Coeffs(15) = 4.91265824826519E-03
    accFilter2_FIR_Coeffs(16) = 4.92004270180173E-02
    accFilter2_FIR_Coeffs(17) = 0.100483509732628
    accFilter2_FIR_Coeffs(18) = 0.148466051793391
    accFilter2_FIR_Coeffs(19) = 0.182538576472018
    accFilter2_FIR_Coeffs(20) = 0.194868478013824
    accFilter2_FIR_Coeffs(21) = 0.182538576472018
    accFilter2_FIR_Coeffs(22) = 0.148466051793391
    accFilter2_FIR_Coeffs(23) = 0.100483509732628
    accFilter2_FIR_Coeffs(24) = 4.92004270180173E-02
    accFilter2_FIR_Coeffs(25) = 4.91265824826519E-03
    accFilter2_FIR_Coeffs(26) = -0.02486029788035
    accFilter2_FIR_Coeffs(27) = -3.73280854708821E-02
    accFilter2_FIR_Coeffs(28) = -3.41726904831107E-02
    accFilter2_FIR_Coeffs(29) = -0.020976137779659
    accFilter2_FIR_Coeffs(30) = -4.23893581240706E-03
    accFilter2_FIR_Coeffs(31) = 9.47028825917822E-03
    accFilter2_FIR_Coeffs(32) = 0.017069344965177
    accFilter2_FIR_Coeffs(33) = 1.67302399377794E-02
    accFilter2_FIR_Coeffs(34) = 1.16405615281411E-02
    accFilter2_FIR_Coeffs(35) = 2.75395332119331E-03
    accFilter2_FIR_Coeffs(36) = -3.60483940737825E-03
    accFilter2_FIR_Coeffs(37) = -9.88942220373758E-03
    accFilter2_FIR_Coeffs(38) = -8.64767900257288E-03
    accFilter2_FIR_Coeffs(39) = -1.04117988138027E-02
    accFilter2_FIR_Coeffs(40) = -5.60304959751038E-03
End Function

Public Function AccFilter2_FilterData(axis As Integer, rawValue As Long) As Long
    Dim filter As Double
    Dim i As Integer
    
    filter = 0
    'Increase number of received data so far
    accFilter2_NumReceivedSamples(axis) = accFilter2_NumReceivedSamples(axis) + 1
    
    For i = cAccFilter2_MaxSamplesInFilter - 1 To 1 Step -1
        accFilter2_Buffer(axis, i) = accFilter2_Buffer(axis, i - 1)
    Next
    accFilter2_Buffer(axis, 0) = CDbl(rawValue)
    
    For i = 0 To cAccFilter2_MaxSamplesInFilter - 1
        filter = filter + accFilter2_Buffer(axis, i) * accFilter2_FIR_Coeffs(i)
    Next
        
    AccFilter2_FilterData = CLng(filter)
End Function
