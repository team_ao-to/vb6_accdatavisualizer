VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmHelp 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Help"
   ClientHeight    =   4440
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   9150
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4440
   ScaleWidth      =   9150
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtHelp 
      Appearance      =   0  'Flat
      Height          =   3660
      Left            =   225
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Top             =   450
      Width           =   8700
   End
   Begin MSComctlLib.TabStrip tabControl 
      Height          =   4335
      Left            =   45
      TabIndex        =   0
      Top             =   45
      Width           =   9060
      _ExtentX        =   15981
      _ExtentY        =   7646
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   3
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Record file"
            Object.ToolTipText     =   "Record file"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Save file"
            Object.ToolTipText     =   "Save file"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab3 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Play file"
            Object.ToolTipText     =   "Play file"
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmHelp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'-----------------------------------------------------------------------------------------------------------------------
'
' (c) Copyright 2016 by Ao-to Corporation. All rights reserved.
'
' The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
' No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
' the specific written permission signed by a legal representative of the Ao-to Corporation.
'
' Change log:
'
' ----------     -----------     -------------     ---------------------------------------------------------------------
' Date           Username        Ticket            Change description
' ----------     -----------     -------------     ---------------------------------------------------------------------
' 19.10.2016     razvan          AOTO2             Initial version of the file
'-----------------------------------------------------------------------------------------------------------------------

Const strHelpRecordFile = "  Start recording a file containing accelerometer data by clicking on the 'File/Record data file' menu in the Scope window." & vbCrLf & _
                          "  A 'Serial communication' window will be displayed, asking you to choose the serial port where the Ao-to G board is connected." & vbCrLf & _
                          "  After choosing the port, if everything works fine, you should see in the 'Serial link' frame in the bottom left side of the Scope window the status 'Connected' displayed in green." & vbCrLf & _
                          "  When you are ready to start recording, press the 'Record' button. The program can record accelerometer samples for a max of 5 minutes." & vbCrLf & _
                          "  Press the 'Stop' button to stop the recording process. At this point, you can:" & vbCrLf & _
                          "  1. Click on the 'File/Save data file' menu to save the captured accelerometer data in a file." & vbCrLf & _
                          "  2. Click on the 'Disconnect' button in order to leave the recording mode and switch to play mode. Now you can play again the captured data information and of course, if you want, you can save it doing the step described above at point #1."

Const strHelpSaveFile = "Save the accelerometer data in a data file by clicking on the 'File/Save data file' menu in the Scope window"
Const strHelpPlayFile = "  Play a file containing accelerometer data by clicking on the 'File/Play data file' menu in the Scope window." & vbCrLf & _
                        "  In the dialog box that appears, choose the path to the data file and click 'Open'. A progress bar is displayed while information from the file is loaded in the program's memory. Once the file is loaded, you can start playing with it:" & vbCrLf & _
                        "  - choose which accelerometer axis data you want to visualize by selecting/de-selecting the checkboxes in the upper left side of the 'Scope' window." & vbCrLf & _
                        "  - zoom in/out on the time axis by scrolling the 'Horizontal zoom' control. The last value used is saved in registry, so next time the program starts, it will use the value set during previous run." & vbCrLf & _
                        "  - zoom in/out on the G axis by scrolling the 'Vertical zoom' control. The last value used is saved in registry, so next time the program starts, it will use the value set during previous run." & vbCrLf & _
                        "  - select the 'AutoSet' checkbox if you want the G axis to automatically resize in order to best fit the accelerometer samples displayed in the Scope window." & vbCrLf & _
                        "  - double click inside the Scope window in order to alternatively set one of the 2 available markers. If you want to hide them, double click on the orange label displaying the current value of the markers and of markers' delta." & vbCrLf & _
                        "  - click on 'Play' button to start plaing the file. Click on 'Pause' to pause the play at the current position." & vbCrLf & _
                        "  - click on 'Stop' button to stop playing the file and bring back the play cursor at begining of the file." & vbCrLf & _
                        "  - single click inside the Scope window in order to set the play cursor's position to the mouse X coordinate. You can do this even when the file is playing, in order to advance the cursor or to move it back in order to replay some part of the file." & vbCrLf & _
                        "  - press the left and right arrow keys when the focus is on the Scope window in order to move the play cursor one data sample to the left or to the right."

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyEscape
            Unload Me
    End Select
End Sub

Private Sub Form_Load()
    txtHelp.Text = strHelpRecordFile
End Sub

Private Sub tabControl_Click()
    Select Case tabControl.SelectedItem
        Case "Record file"
            txtHelp.Text = strHelpRecordFile
        Case "Save file"
            txtHelp.Text = strHelpSaveFile
        Case "Play file"
            txtHelp.Text = strHelpPlayFile
    End Select
    
End Sub

Private Sub txtHelp_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyEscape
            Unload Me
    End Select

End Sub
