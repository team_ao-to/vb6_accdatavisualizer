VERSION 5.00
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSCOMM32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{55473EAC-7715-4257-B5EF-6E14EBD6A5DD}#1.0#0"; "vbalProgBar6.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmScopeView 
   Caption         =   "Scope"
   ClientHeight    =   7935
   ClientLeft      =   120
   ClientTop       =   720
   ClientWidth     =   8070
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7935
   ScaleWidth      =   8070
   Begin MSComDlg.CommonDialog commonDialog 
      Left            =   4680
      Top             =   675
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame frameSerial 
      Caption         =   "Serial link"
      Height          =   1140
      Left            =   45
      TabIndex        =   19
      Top             =   6030
      Width           =   1815
      Begin VB.CommandButton cmdDisconnect 
         Caption         =   "Disconnect"
         Height          =   330
         Left            =   450
         TabIndex        =   21
         Top             =   675
         Visible         =   0   'False
         Width           =   1140
      End
      Begin VB.Label lblSerialStatus 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         Caption         =   "Disconnected"
         Height          =   210
         Left            =   480
         TabIndex        =   20
         Top             =   225
         Width           =   1125
      End
      Begin VB.Shape shapeSerialStatus 
         BorderStyle     =   0  'Transparent
         FillColor       =   &H000000FF&
         FillStyle       =   0  'Solid
         Height          =   195
         Left            =   90
         Shape           =   3  'Circle
         Top             =   495
         Visible         =   0   'False
         Width           =   375
      End
   End
   Begin VB.Frame frameControl 
      Caption         =   "Control"
      Height          =   1410
      Left            =   45
      TabIndex        =   16
      Top             =   4590
      Width           =   1815
      Begin vbalProgBarLib6.vbalProgressBar progressPlay 
         Height          =   195
         Left            =   90
         TabIndex        =   25
         Top             =   1080
         Width           =   1590
         _ExtentX        =   2805
         _ExtentY        =   344
         Picture         =   "frmScopeView.frx":0000
         ForeColor       =   0
         BarPicture      =   "frmScopeView.frx":001C
         ShowText        =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CommandButton cmdStop 
         Caption         =   "Stop"
         Height          =   420
         Left            =   900
         TabIndex        =   18
         Top             =   315
         Width           =   825
      End
      Begin VB.CommandButton cmdRecStopPlayPause 
         Caption         =   "Play"
         Height          =   420
         Left            =   90
         TabIndex        =   17
         Top             =   315
         Width           =   780
      End
      Begin VB.Label lblTime 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   90
         TabIndex        =   24
         Top             =   810
         Width           =   1650
      End
   End
   Begin MSCommLib.MSComm comPort 
      Left            =   3780
      Top             =   90
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DTREnable       =   -1  'True
   End
   Begin VB.VScrollBar VScroll 
      Height          =   2175
      Left            =   6165
      Max             =   0
      TabIndex        =   9
      Top             =   45
      Width           =   285
   End
   Begin VB.HScrollBar HScroll 
      Height          =   240
      Left            =   1935
      Max             =   0
      TabIndex        =   7
      Top             =   2475
      Width           =   4470
   End
   Begin VB.Timer tmrScopeViewRefresh 
      Interval        =   50
      Left            =   3735
      Top             =   720
   End
   Begin VB.PictureBox picPanel 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      DrawWidth       =   2
      ForeColor       =   &H000000FF&
      Height          =   1035
      Index           =   0
      Left            =   1935
      ScaleHeight     =   1005
      ScaleWidth      =   1545
      TabIndex        =   4
      Top             =   90
      Width           =   1575
      Begin VB.Label lblScopeViewMax 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Max: "
         Height          =   210
         Left            =   0
         TabIndex        =   23
         Top             =   0
         Width           =   435
      End
      Begin VB.Label lblScopeViewMin 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Min:"
         Height          =   210
         Left            =   0
         TabIndex        =   22
         Top             =   315
         Width           =   330
      End
   End
   Begin VB.Frame frameDisplay 
      Caption         =   "Display"
      Height          =   4560
      Left            =   45
      TabIndex        =   0
      Top             =   0
      Width           =   1860
      Begin VB.CheckBox chkShowRawAcc 
         Caption         =   "Raw Z:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00B6B8F5&
         Height          =   285
         Index           =   2
         Left            =   45
         TabIndex        =   13
         Top             =   1350
         Value           =   1  'Checked
         Width           =   1770
      End
      Begin VB.CheckBox chkShowRawAcc 
         Caption         =   "Raw Y:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0068DD86&
         Height          =   285
         Index           =   1
         Left            =   45
         TabIndex        =   12
         Top             =   900
         Value           =   1  'Checked
         Width           =   1770
      End
      Begin VB.CheckBox chkShowRawAcc 
         Caption         =   "Raw X:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00F9CC71&
         Height          =   285
         Index           =   0
         Left            =   45
         TabIndex        =   11
         Top             =   450
         Value           =   1  'Checked
         Width           =   1770
      End
      Begin VB.CheckBox chkAutoSetY 
         Caption         =   "AutoSet"
         Height          =   285
         Left            =   585
         TabIndex        =   10
         Top             =   2880
         Width           =   1140
      End
      Begin MSComctlLib.Slider slideHorizontal 
         Height          =   420
         Left            =   90
         TabIndex        =   5
         Top             =   1620
         Width           =   1680
         _ExtentX        =   2963
         _ExtentY        =   741
         _Version        =   393216
         LargeChange     =   1
         Min             =   1
         Max             =   1000
         SelStart        =   10
         TickStyle       =   3
         Value           =   10
      End
      Begin VB.CheckBox chkShowFilteredAcc 
         Caption         =   "Acc Z:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   285
         Index           =   2
         Left            =   45
         TabIndex        =   3
         Top             =   1125
         Value           =   1  'Checked
         Width           =   1770
      End
      Begin VB.CheckBox chkShowFilteredAcc 
         Caption         =   "Acc Y:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   285
         Index           =   1
         Left            =   45
         TabIndex        =   2
         Top             =   675
         Value           =   1  'Checked
         Width           =   1770
      End
      Begin VB.CheckBox chkShowFilteredAcc 
         Caption         =   "Acc X:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   285
         Index           =   0
         Left            =   45
         TabIndex        =   1
         Top             =   225
         Value           =   1  'Checked
         Width           =   1770
      End
      Begin MSComctlLib.Slider slideVertical 
         Height          =   1320
         Left            =   90
         TabIndex        =   6
         Top             =   2205
         Width           =   420
         _ExtentX        =   741
         _ExtentY        =   2328
         _Version        =   393216
         Orientation     =   1
         LargeChange     =   1
         Min             =   1
         Max             =   49800
         TickStyle       =   3
      End
      Begin VB.Label lblVerticalZoom 
         AutoSize        =   -1  'True
         Caption         =   "Vertical zoom"
         Height          =   210
         Left            =   585
         TabIndex        =   15
         Top             =   2565
         Width           =   1095
      End
      Begin VB.Label lblHorizontalZoom 
         AutoSize        =   -1  'True
         Caption         =   "Horizontal zoom"
         Height          =   210
         Left            =   450
         TabIndex        =   14
         Top             =   2025
         Width           =   1290
      End
      Begin VB.Label lblMarkers 
         Caption         =   "Horizontal Markers:"
         ForeColor       =   &H000080FF&
         Height          =   960
         Left            =   45
         TabIndex        =   8
         Top             =   3510
         Width           =   1635
      End
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuRecordFile 
         Caption         =   "&Record data file"
      End
      Begin VB.Menu mnuSaveFile 
         Caption         =   "&Save data file"
      End
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Play data file"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuShowHelp 
         Caption         =   "Show help"
      End
   End
End
Attribute VB_Name = "frmScopeView"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'-----------------------------------------------------------------------------------------------------------------------
'
' (c) Copyright 2016 by Ao-to Corporation. All rights reserved.
'
' The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
' No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
' the specific written permission signed by a legal representative of the Ao-to Corporation.
'
' Change log:
'
' ----------     -----------     -------------     ---------------------------------------------------------------------
' Date           Username        Ticket            Change description
' ----------     -----------     -------------     ---------------------------------------------------------------------
' 19.10.2016     razvan          AOTO2             Initial version of the file
'-----------------------------------------------------------------------------------------------------------------------

Option Explicit

Private WithEvents tmrPlay As cHiResTimer
Attribute tmrPlay.VB_VarHelpID = -1

Public Enum frmShowReason_e
    efrmShowReason_JustLoaded = 0
    efrmShowReason_PlayFile = 1
    efrmShowReason_RecordFile = 2
End Enum


Const cFormVerticalMargin = 900
Const cFormHorizontalMargin = 350

'Variables storing the scope view window sizes in twips
Dim scopeViewHeightInTwips As Long
Dim scopeViewWidthInTwips As Long

Public scopeViewPlayedFileDir As String

Dim minScopeView As Long
Dim maxScopeView As Long

Dim angle As Double

Dim yDblClick As Single
Dim xSingleClick As Single

'Reason why this form is showned
Private frmShowReason As frmShowReason_e

'Maximum number of samples that can be printed in scope window. It is basically the window width divided with width of one sample
Private maxNumSamplesThatCanBePrintedInWindow As Long
'Number of data samples in the scope window. Usually, when the recording starts for example, this number is lower than maxNumSamplesThatCanBePrintedInWindow
'but increases until it gets to maxNumSamplesThatCanBePrintedInWindow
Private numDataSamplesInWindow As Long
'The index in the circular buffers of first data sample that is displayed in the scope window
Private idxStartDataSampleInWindow As Long

Private Sub chkAutoSetY_Click()
    If chkAutoSetY.value = vbChecked Then
        VScroll.Enabled = False
        slideVertical.Enabled = False
    Else
        VScroll.Enabled = True
        slideVertical.Enabled = True
    End If
End Sub

Private Sub cmdDisconnect_Click()
    Serial_CloseComPort
    ScopeView_ConfigureGUIControls efrmShowReason_PlayFile
End Sub

Private Sub cmdRecStopPlayPause_Click()
    Select Case frmShowReason
        Case efrmShowReason_PlayFile
            If cmdRecStopPlayPause.Caption = "Play" Then
                ' It's time to start playing a file
                cmdRecStopPlayPause.Caption = "Pause"
                cmdStop.Visible = False
                tmrPlay.Enabled("tmrPlay") = True
            Else
                ' It's time to pause
                cmdRecStopPlayPause.Caption = "Play"
                cmdStop.Visible = True
                tmrPlay.Enabled("tmrPlay") = False
            End If
        Case efrmShowReason_RecordFile
            If cmdRecStopPlayPause.Caption = "Record" Then
                ' It's time to start recording data
                ' Reset scope circular buffers
                Scope_Init
                cmdRecStopPlayPause.Caption = "Stop"
                Serial_StartRecordingData
            Else
                'It's time to stop recording data
                Serial_StopRecordingData
                cmdRecStopPlayPause.Caption = "Record"
            End If
    End Select
End Sub



Private Sub cmdStop_Click()
    scopeCurrentPlayedOrRecordedDataSample = scopeCircularBufferTail
    ScopeView_SetWindowStartingFromDataSample scopeCurrentPlayedOrRecordedDataSample
    ScopeView_UpdatePlayProgress
End Sub


Private Sub comPort_OnComm()
    On Error Resume Next
    Select Case comPort.CommEvent
        Case comEvReceive
            Serial_RxCallback
    End Select
End Sub

Private Sub Form_Load()
    Registry_GetFormPosition Me, True
    Me.Caption = "AccDataVisualizer v" & App.Major & "." & App.Minor & "." & App.Revision
    ScopeView_Init
    ScopeView_ConfigureGUIControls efrmShowReason_JustLoaded
    ScopeView_ResizeFormControls
    Set tmrPlay = New cHiResTimer
    
    'Configure the timer to fire every 10 ms. This is the interval used to capture data from accelerometer
    
    'MARKER - Apparently the timer does not fire at the programmed interval, it is delayed. So in order to obtain a 10 ms
    'interval between the ticks, it needs to be programmed with a lower interval. This might depend on the speed of the machine
    'the software runs on. Adjust a value here, than play a file and look how the time increases in the play window and compare it
    'with a watch time or something, to see if the value adjusted here is correct or needs more tuning
    tmrPlay.Add "tmrPlay", 8, False
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    On Error Resume Next
    Registry_SetFormPosition Me, True
    'Store all control values in registry
    SaveSetting App.Title, "Settings", Me.Name & "chkShowFilteredAcc0", chkShowFilteredAcc(0).value
    SaveSetting App.Title, "Settings", Me.Name & "chkShowFilteredAcc1", chkShowFilteredAcc(1).value
    SaveSetting App.Title, "Settings", Me.Name & "chkShowFilteredAcc2", chkShowFilteredAcc(2).value
    SaveSetting App.Title, "Settings", Me.Name & "chkShowRawAcc0", chkShowRawAcc(0).value
    SaveSetting App.Title, "Settings", Me.Name & "chkShowRawAcc1", chkShowRawAcc(1).value
    SaveSetting App.Title, "Settings", Me.Name & "chkShowRawAcc2", chkShowRawAcc(2).value
    SaveSetting App.Title, "Settings", Me.Name & "chkAutoSetY", chkAutoSetY.value
    SaveSetting App.Title, "Settings", Me.Name & "slideHorizontal", slideHorizontal.value
    SaveSetting App.Title, "Settings", Me.Name & "slideVertical", slideVertical.value
    ' Close all forms
    Main_CloseAllForms
End Sub

Private Sub Form_Resize()
    ScopeView_ResizeFormControls
    maxNumSamplesThatCanBePrintedInWindow = scopeViewWidthInTwips \ slideHorizontal.value
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyEscape
            Unload Me
    End Select
End Sub

Private Sub ScopeView_Init()
    'Initialize the 'view' of the scope to be able to display the max range of values
    minScopeView = cScopeMinDisplayedValue
    maxScopeView = cScopeMaxDisplayedValue
  
    bScopeMarkerVisible(0) = False
    bScopeMarkerVisible(1) = False
    scopeLastMarker = 0
    
    lblMarkers.Caption = "Horizontal Markers:" & vbCrLf & _
                         "1: Dbl click to set" & vbCrLf & _
                         "2: Dbl click to set" & vbCrLf & _
                         "D: -"
    'Restore all control values from registry
    chkShowFilteredAcc(0).value = GetSetting(App.Title, "Settings", Me.Name & "chkShowFilteredAcc0", vbChecked)
    chkShowFilteredAcc(1).value = GetSetting(App.Title, "Settings", Me.Name & "chkShowFilteredAcc1", vbChecked)
    chkShowFilteredAcc(2).value = GetSetting(App.Title, "Settings", Me.Name & "chkShowFilteredAcc2", vbChecked)
    chkShowRawAcc(0).value = GetSetting(App.Title, "Settings", Me.Name & "chkShowRawAcc0", vbChecked)
    chkShowRawAcc(1).value = GetSetting(App.Title, "Settings", Me.Name & "chkShowRawAcc1", vbChecked)
    chkShowRawAcc(2).value = GetSetting(App.Title, "Settings", Me.Name & "chkShowRawAcc2", vbChecked)
    chkAutoSetY.value = GetSetting(App.Title, "Settings", Me.Name & "chkAutoSetY", vbUnchecked)
    slideHorizontal.value = GetSetting(App.Title, "Settings", Me.Name & "slideHorizontal", 36)
    slideVertical.value = GetSetting(App.Title, "Settings", Me.Name & "slideVertical", 0)
End Sub

Public Sub ScopeView_ConfigureGUIControls(reason As frmShowReason_e)
    frmShowReason = reason
    Select Case frmShowReason
        Case efrmShowReason_JustLoaded
            frameControl.Visible = False
            frameSerial.Visible = False
        Case efrmShowReason_PlayFile
            frameControl.Visible = True
            frameControl.Caption = "Playing"
            frameSerial.Visible = False
            cmdRecStopPlayPause.Caption = "Play"
            cmdStop.Visible = True
            progressPlay.Visible = True
        Case efrmShowReason_RecordFile
            frameControl.Visible = True
            frameControl.Caption = "Recording"
            frameSerial.Visible = True
            cmdRecStopPlayPause.Caption = "Record"
            cmdStop.Visible = False
    End Select
End Sub

Public Sub ScopeView_SetWindowStartingFromDataSample(idxDataSample As Long)
    ' If there is not at least 2 values in the buffers, exit
    If scopeNumDataPointsInCircularBuffers < 2 Then
        Exit Sub
    End If

    ' Compute max number of samples that we can print
    If (scopeNumDataPointsInCircularBuffers <= maxNumSamplesThatCanBePrintedInWindow) Then
        idxStartDataSampleInWindow = scopeCircularBufferTail
        numDataSamplesInWindow = scopeNumDataPointsInCircularBuffers
    Else
        If (Scope_GetDataSampleDistanceFromTail(idxDataSample) < maxNumSamplesThatCanBePrintedInWindow / 2) Then
            idxStartDataSampleInWindow = scopeCircularBufferTail
        ElseIf (Scope_GetDataSampleDistanceFromHead(idxDataSample) < maxNumSamplesThatCanBePrintedInWindow / 2) Then
            idxStartDataSampleInWindow = Scope_CircularSubstract(scopeCircularBufferHead, maxNumSamplesThatCanBePrintedInWindow)
        Else
            idxStartDataSampleInWindow = Scope_CircularSubstract(idxDataSample, maxNumSamplesThatCanBePrintedInWindow / 2)
        End If
        numDataSamplesInWindow = maxNumSamplesThatCanBePrintedInWindow
    End If
End Sub



Public Sub ScopeView_RedrawAnalyzerData()
    Dim i As Long, xCoordinate As Long
    Dim lastPrintedData As Long
    Dim maxNumSamplesThatCanBePrinted As Long
    Dim idxDataInCircularBuffer As Long
    Dim step As Long
    
    
    
    If chkAutoSetY.value = vbChecked Then
        ScopeView_AutoCalibrateY
    End If
    
    picPanel(0).Cls
    
    'Print markers
    picPanel(0).DrawStyle = vbDash
    picPanel(0).ForeColor = &H80FF&
    picPanel(0).DrawWidth = 1
    If bScopeMarkerVisible(0) Then
        picPanel(0).Line (0, scopeViewHeightInTwips - ScopeView_GetY(scopeMarkerVal(0)))-(scopeViewWidthInTwips, scopeViewHeightInTwips - ScopeView_GetY(scopeMarkerVal(0)))
    End If
    If bScopeMarkerVisible(1) Then
        picPanel(0).Line (0, scopeViewHeightInTwips - ScopeView_GetY(scopeMarkerVal(1)))-(scopeViewWidthInTwips, scopeViewHeightInTwips - ScopeView_GetY(scopeMarkerVal(1)))
    End If
    picPanel(0).DrawStyle = vbSolid
    picPanel(0).DrawWidth = 2
    
    ' Print min and max
    lblScopeViewMax = "Max: " & Format(maxScopeView * 0.06, "##0.00") & " mG"
    lblScopeViewMin = "Min: " & Format(minScopeView * 0.06, "##0.00") & " mG"

    ' If there is not at least 2 values in the buffers, exit
    If scopeNumDataPointsInCircularBuffers < 2 Then
        Exit Sub
    End If
    
    ' Display status of serial link
    If frmShowReason = efrmShowReason_RecordFile Then
        If Serial_NumMissedPackets <> 0 Then
            lblSerialStatus.Caption = "Packet miss" & vbCrLf & CStr(Serial_NumMissedPackets)
            lblSerialStatus.ForeColor = vbRed
            shapeSerialStatus.FillColor = vbRed
        Else
            lblSerialStatus.Caption = "Connected"
            lblSerialStatus.ForeColor = vbGreen
            shapeSerialStatus.FillColor = vbGreen
        End If
    End If
    
    'We can only afford to draw a max of 500 line segments per each displayed parameter. Drawing more than this will make the program move slow
    If numDataSamplesInWindow > 500 Then
        step = numDataSamplesInWindow \ 500
    Else
        step = 1
    End If
    
    ' Print data
    xCoordinate = 0
    For i = idxStartDataSampleInWindow To idxStartDataSampleInWindow + numDataSamplesInWindow - 2
        If i Mod step = 0 Then
            ScopeView_PrintDataPoint xCoordinate, step, i
        End If
        'Print currently playing/recording dataPoint
        If i = scopeCurrentPlayedOrRecordedDataSample Then
            picPanel(0).DrawWidth = 1
            picPanel(0).DrawStyle = vbDash
            picPanel(0).ForeColor = vbBlack
            picPanel(0).Line (xCoordinate * slideHorizontal.value, 0)-(xCoordinate * slideHorizontal.value, scopeViewHeightInTwips)
            picPanel(0).DrawWidth = 2
            picPanel(0).DrawStyle = vbSolid
        End If
        xCoordinate = xCoordinate + 1
    Next
End Sub

Private Sub ScopeView_PrintDataPoint(xCoordinate As Long, step As Long, idxDataPoint As Long)
    Dim idxDataPointEnd As Long
    idxDataPointEnd = Scope_CircularAdd(idxDataPoint, step)
    
    If chkShowRawAcc(0).value = vbChecked Then
        picPanel(0).ForeColor = &HF9CC71
        picPanel(0).Line (xCoordinate * slideHorizontal.value, scopeViewHeightInTwips - ScopeView_GetY(scopeCircularBufferAccDataRaw(0, idxDataPoint)))-((xCoordinate + step) * slideHorizontal.value, scopeViewHeightInTwips - ScopeView_GetY(scopeCircularBufferAccDataRaw(0, idxDataPointEnd)))
    End If
    If chkShowFilteredAcc(0).value = vbChecked Then
        picPanel(0).ForeColor = &HFF0000
        picPanel(0).Line (xCoordinate * slideHorizontal.value, scopeViewHeightInTwips - ScopeView_GetY(scopeCircularBufferAccDataFiltered(0, idxDataPoint)))-((xCoordinate + step) * slideHorizontal.value, scopeViewHeightInTwips - ScopeView_GetY(scopeCircularBufferAccDataFiltered(0, idxDataPointEnd)))
    End If
    
    If chkShowRawAcc(1).value = vbChecked Then
        picPanel(0).ForeColor = &H68DD86
        picPanel(0).Line (xCoordinate * slideHorizontal.value, scopeViewHeightInTwips - ScopeView_GetY(scopeCircularBufferAccDataRaw(1, idxDataPoint)))-((xCoordinate + step) * slideHorizontal.value, scopeViewHeightInTwips - ScopeView_GetY(scopeCircularBufferAccDataRaw(1, idxDataPointEnd)))
    End If
    If chkShowFilteredAcc(1).value = vbChecked Then
        picPanel(0).ForeColor = &H8000&
        picPanel(0).Line (xCoordinate * slideHorizontal.value, scopeViewHeightInTwips - ScopeView_GetY(scopeCircularBufferAccDataFiltered(1, idxDataPoint)))-((xCoordinate + step) * slideHorizontal.value, scopeViewHeightInTwips - ScopeView_GetY(scopeCircularBufferAccDataFiltered(1, idxDataPointEnd)))
    End If
    
    If chkShowRawAcc(2).value = vbChecked Then
        picPanel(0).ForeColor = &HB6B8F5
        picPanel(0).Line (xCoordinate * slideHorizontal.value, scopeViewHeightInTwips - ScopeView_GetY(scopeCircularBufferAccDataRaw(2, idxDataPoint)))-((xCoordinate + step) * slideHorizontal.value, scopeViewHeightInTwips - ScopeView_GetY(scopeCircularBufferAccDataRaw(2, idxDataPointEnd)))
    End If
    If chkShowFilteredAcc(2).value = vbChecked Then
        picPanel(0).ForeColor = &HFF&
        picPanel(0).Line (xCoordinate * slideHorizontal.value, scopeViewHeightInTwips - ScopeView_GetY(scopeCircularBufferAccDataFiltered(2, idxDataPoint)))-((xCoordinate + step) * slideHorizontal.value, scopeViewHeightInTwips - ScopeView_GetY(scopeCircularBufferAccDataFiltered(2, idxDataPointEnd)))
    End If
End Sub

Private Function ScopeView_GetY(data As Long) As Long
    Dim res As Double
    res = CDbl(scopeViewHeightInTwips) * CDbl((data - minScopeView)) / CDbl((maxScopeView - minScopeView))
    ScopeView_GetY = CLng(res)
End Function

Private Function ScopeView_GetFilteredTailVal(posFromTail As Long, axis As Integer) As Long
    ScopeView_GetFilteredTailVal = scopeCircularBufferAccDataFiltered(axis, Scope_GetTailPosition(posFromTail))
End Function

Private Function ScopeView_GetNotFilteredTailVal(posFromTail As Long, axis As Integer) As Long
    ScopeView_GetNotFilteredTailVal = scopeCircularBufferAccDataRaw(axis, Scope_GetTailPosition(posFromTail))
End Function



Private Sub ScopeView_ResizeFormControls()
    On Error Resume Next
   
    scopeViewHeightInTwips = Me.height - picPanel(0).Top - HScroll.height - cFormVerticalMargin
    scopeViewWidthInTwips = Me.width - picPanel(0).Left - VScroll.width - cFormHorizontalMargin
    
    HScroll.Left = picPanel(0).Left
    HScroll.width = Me.width - HScroll.Left - cFormHorizontalMargin
    HScroll.Top = Me.height - HScroll.height - cFormVerticalMargin
    
    VScroll.Left = picPanel(0).Left + picPanel(0).width
    VScroll.height = Me.height - HScroll.height - cFormVerticalMargin
    
    lblScopeViewMin.Top = scopeViewHeightInTwips - lblScopeViewMin.height
    
    'Resize window if length is larger than number of samples
    If scopeViewWidthInTwips > cScopeMaxNumSamples Then
        Me.width = picPanel(0).Left + cFormHorizontalMargin + cScopeMaxNumSamples
    End If
    
    picPanel(0).height = scopeViewHeightInTwips
    picPanel(0).width = scopeViewWidthInTwips
End Sub

Private Sub HScroll_Change()
    idxStartDataSampleInWindow = Scope_CircularAdd(scopeCircularBufferTail, HScroll.value)
End Sub

Private Sub HScroll_Scroll()
    idxStartDataSampleInWindow = Scope_CircularAdd(scopeCircularBufferTail, HScroll.value)
End Sub


Private Sub lblMarkers_DblClick()
    bScopeMarkerVisible(0) = False
    bScopeMarkerVisible(1) = False
    lblMarkers.Caption = "Horizontal Markers:" & vbCrLf & _
                         "1: Dbl click to set" & vbCrLf & _
                         "2: Dbl click to set" & vbCrLf & _
                         "D: -"
End Sub

Private Sub mnuFileOpen_Click()
    Dim FSO
    Set FSO = CreateObject("Scripting.FileSystemObject")
    commonDialog.InitDir = GetSetting(App.Title, "Settings", Me.Name & "ScopeViewFileOpenDir", "C:\")
    commonDialog.DialogTitle = "Please select data samples file to open"
    commonDialog.filter = "All Files (*.*)|*.*"
    commonDialog.ShowOpen
    'If user did not select anything, exit procedure
    If commonDialog.FileName = "" Then Exit Sub
        
    'Save the path to the opened file in registry
    SaveSetting App.Title, "Settings", Me.Name & "ScopeViewFileOpenDir", commonDialog.FileName
    
    scopeViewPlayedFileDir = FSO.GetParentFolderName(commonDialog.FileName)
    
    Scope_Init
    ScopeView_ConfigureGUIControls efrmShowReason_PlayFile
    TextFile_ReadFile commonDialog.FileName
    ScopeView_UpdatePlayProgress
End Sub

Private Sub mnuRecordFile_Click()
    frmComConfig.Show 1
    
End Sub

Private Sub mnuSaveFile_Click()
    Dim FSO As Scripting.FileSystemObject
    'Create a new FileSystemObject
    Set FSO = New Scripting.FileSystemObject
    Dim res As Integer

    commonDialog.InitDir = GetSetting(App.Title, "Settings", Me.Name & "ScopeViewFileOpenDir", "C:\")
    commonDialog.DialogTitle = "Please select the name of the data samples file to save"
    commonDialog.filter = "Text Files (*.txt)|*.txt"
    commonDialog.ShowSave
    'If user did not select anything, exit procedure
    If commonDialog.FileName = "" Then Exit Sub
    'Save the path to the opened file in registry
    SaveSetting App.Title, "Settings", Me.Name & "ScopeViewFileOpenDir", commonDialog.FileName

    If FSO.FileExists(commonDialog.FileName) Then
        res = MsgBox("The file already exists. Do you want to overwrite it?", vbYesNo + vbQuestion + vbDefaultButton1, "Overwrite?")
        If res = vbYes Then
            TextFile_WriteFile commonDialog.FileName
        End If
    Else
        TextFile_WriteFile commonDialog.FileName
    End If
    
    Set FSO = Nothing
End Sub


Private Sub mnuShowHelp_Click()
    frmHelp.Show 1
End Sub

Private Sub picPanel_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If frmShowReason <> efrmShowReason_PlayFile Then Exit Sub
    
    Select Case KeyCode
        Case vbKeyLeft
            If (scopeCurrentPlayedOrRecordedDataSample <> scopeCircularBufferTail) Then
                'Advance to next sample
                scopeCurrentPlayedOrRecordedDataSample = Scope_CircularSubstract(scopeCurrentPlayedOrRecordedDataSample, 1)
                'Refresh all data
                ScopeView_RefreshData
            End If
        Case vbKeyRight
            If (scopeCurrentPlayedOrRecordedDataSample <> scopeCircularBufferHead) Then
                'Go back to previous sample
                scopeCurrentPlayedOrRecordedDataSample = Scope_CircularAdd(scopeCurrentPlayedOrRecordedDataSample, 1)
                'Refresh all data
                ScopeView_RefreshData
            End If
    End Select
End Sub


Private Sub picPanel_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    yDblClick = Y
    xSingleClick = X
End Sub

' PicPanel events
Private Sub picPanel_Click(Index As Integer)
    If frmShowReason <> efrmShowReason_PlayFile Then Exit Sub
    
    Dim idxClickedSample As Long
    idxClickedSample = xSingleClick \ slideHorizontal.value
    idxClickedSample = idxStartDataSampleInWindow + idxClickedSample
    If idxClickedSample > scopeNumDataPointsInCircularBuffers Then
        Exit Sub
    End If
    'Set current played sample to the clicked one
    scopeCurrentPlayedOrRecordedDataSample = idxClickedSample
    'Refresh all data
    ScopeView_RefreshData
End Sub

Private Sub picPanel_DblClick(Index As Integer)
    Dim delta As Long
    
    If bScopeMarkerVisible(0) = False Then
        scopeMarkerVal(0) = CLng((minScopeView + CDbl((scopeViewHeightInTwips - yDblClick)) / CDbl(scopeViewHeightInTwips) * CDbl((maxScopeView - minScopeView))))
        bScopeMarkerVisible(0) = True
        lblMarkers.Caption = "Horizontal Markers:" & vbCrLf & _
                             "1: " & Format(scopeMarkerVal(0) * 0.06, "##0.00") & "mG" & vbCrLf & _
                             "2: Dbl click to set" & vbCrLf & _
                             "D: -"
    ElseIf bScopeMarkerVisible(1) = False Then
        scopeMarkerVal(1) = CLng((minScopeView + CDbl((scopeViewHeightInTwips - yDblClick)) / CDbl(scopeViewHeightInTwips) * CDbl((maxScopeView - minScopeView))))
        bScopeMarkerVisible(1) = True
        If scopeMarkerVal(0) < scopeMarkerVal(1) Then
            delta = scopeMarkerVal(1) - scopeMarkerVal(0)
        Else
            delta = scopeMarkerVal(0) - scopeMarkerVal(1)
        End If
        lblMarkers.Caption = "Markers" & vbCrLf & _
                             "1: " & Format(scopeMarkerVal(0) * 0.06, "##0.00") & " mG" & vbCrLf & _
                             "2: " & Format(scopeMarkerVal(1) * 0.06, "##0.00") & " mG" & vbCrLf & _
                             "D: " & Format(delta * 0.06, "##0.00") & " mG"
    Else
        scopeLastMarker = 1 - scopeLastMarker
        scopeMarkerVal(scopeLastMarker) = CLng((minScopeView + CDbl((scopeViewHeightInTwips - yDblClick)) / CDbl(scopeViewHeightInTwips) * CDbl((maxScopeView - minScopeView))))
        If scopeMarkerVal(0) < scopeMarkerVal(1) Then
            delta = scopeMarkerVal(1) - scopeMarkerVal(0)
        Else
            delta = scopeMarkerVal(0) - scopeMarkerVal(1)
        End If
        lblMarkers.Caption = "Markers" & vbCrLf & _
                             "1: " & Format(scopeMarkerVal(0) * 0.06, "##0.00") & " mG" & vbCrLf & _
                             "2: " & Format(scopeMarkerVal(1) * 0.06, "##0.00") & " mG" & vbCrLf & _
                             "D: " & Format(delta * 0.06, "##0.00") & " mG"
        
    End If
End Sub

Public Sub ScopeView_UpdateHScrollMaxValue()
    If scopeNumDataPointsInCircularBuffers <= maxNumSamplesThatCanBePrintedInWindow Then
        frmScopeView.HScroll.Max = 0
        frmScopeView.HScroll.value = 0
    Else
        frmScopeView.HScroll.Max = scopeNumDataPointsInCircularBuffers - maxNumSamplesThatCanBePrintedInWindow
        frmScopeView.HScroll.value = scopeNumDataPointsInCircularBuffers - maxNumSamplesThatCanBePrintedInWindow
    End If
End Sub

Private Sub slideHorizontal_Scroll()
    maxNumSamplesThatCanBePrintedInWindow = scopeViewWidthInTwips \ slideHorizontal.value
    ScopeView_SetWindowStartingFromDataSample scopeCurrentPlayedOrRecordedDataSample
    ScopeView_UpdateHScrollMaxValue
End Sub



Private Sub slideVertical_Change()
    Call slideVertical_Scroll
End Sub

Private Sub slideVertical_Scroll()
    Dim scopeViewHeight As Long
    Dim scopeViewMiddleY As Long
    scopeViewHeight = 50000 - slideVertical.value ' (cScopeMaxDisplayedValue - cScopeMinDisplayedValue)
    
    'try to stay arround old middle y
    If scopeViewHeight = 50000 - 1 Then
        scopeViewMiddleY = 0
    Else
        scopeViewMiddleY = (maxScopeView + minScopeView) / 2
    End If
    
    maxScopeView = scopeViewMiddleY + scopeViewHeight / 2
    minScopeView = scopeViewMiddleY - scopeViewHeight / 2
    
    If (maxScopeView > cScopeMaxDisplayedValue) Then
        maxScopeView = cScopeMaxDisplayedValue
    End If
    If (minScopeView < cScopeMinDisplayedValue) Then
        minScopeView = cScopeMinDisplayedValue
    End If
    
    VScroll.Max = (50000 - (maxScopeView - minScopeView)) / 10
    VScroll.value = VScroll.Max / 2
End Sub

Private Sub VScroll_Scroll()
    Dim deltaScopeView As Long
    deltaScopeView = maxScopeView - minScopeView
    minScopeView = cScopeMinDisplayedValue + CLng(VScroll.Max - VScroll.value) * 10 '- deltaScopeView / 2
    maxScopeView = minScopeView + deltaScopeView

    lblScopeViewMax = "Max: " & Format(maxScopeView * 0.06, "##0.00") & " mG"
    lblScopeViewMin = "Min: " & Format(minScopeView * 0.06, "##0.00") & " mG"
End Sub

Private Sub tmrPlay_Timer(ByVal sKey As String)
    If (scopeCurrentPlayedOrRecordedDataSample = Scope_CircularSubstract(scopeCircularBufferHead, 1)) Then
        'We're at the end of the file
        cmdRecStopPlayPause.Caption = "Play"
        cmdStop.Visible = True
        tmrPlay.Enabled("tmrPlay") = False
        scopeCurrentPlayedOrRecordedDataSample = Scope_CircularSubstract(scopeCurrentPlayedOrRecordedDataSample, 1)
        
        Exit Sub
    End If
    'Refresh all data
    ScopeView_RefreshData
    'Advance to next sample
    scopeCurrentPlayedOrRecordedDataSample = Scope_CircularAdd(scopeCurrentPlayedOrRecordedDataSample, 1)
End Sub

Public Sub ScopeView_UpdateAccLabels()
    chkShowFilteredAcc(0).Caption = "Acc X:" & Format(scopeCircularBufferAccDataFiltered(cAcc_AxisX, scopeCurrentPlayedOrRecordedDataSample) * 0.06, "##0.00") & "mG"
    chkShowRawAcc(0).Caption = "Raw X:" & Format(scopeCircularBufferAccDataRaw(cAcc_AxisX, scopeCurrentPlayedOrRecordedDataSample) * 0.06, "##0.00") & "mG"

    chkShowFilteredAcc(1).Caption = "Acc Y:" & Format(scopeCircularBufferAccDataFiltered(cAcc_AxisX, scopeCurrentPlayedOrRecordedDataSample) * 0.06, "##0.00") & "mG"
    chkShowRawAcc(1).Caption = "Raw Y:" & Format(scopeCircularBufferAccDataRaw(cAcc_AxisY, scopeCurrentPlayedOrRecordedDataSample) * 0.06, "##0.00") & "mG"

    chkShowFilteredAcc(2).Caption = "Acc Z:" & Format(scopeCircularBufferAccDataFiltered(cAcc_AxisZ, scopeCurrentPlayedOrRecordedDataSample) * 0.06, "##0.00") & "mG"
    chkShowRawAcc(2).Caption = "Raw Z:" & Format(scopeCircularBufferAccDataRaw(cAcc_AxisZ, scopeCurrentPlayedOrRecordedDataSample) * 0.06, "##0.00") & "mG"
End Sub
    

Public Sub ScopeView_UpdatePlayProgress()
    On Error Resume Next
    progressPlay.Max = scopeNumDataPointsInCircularBuffers
    progressPlay.value = Scope_GetDataSampleDistanceFromTail(scopeCurrentPlayedOrRecordedDataSample)
    progressPlay.Text = Format(progressPlay.value / progressPlay.Max * 100, "##0.00") & "%"
    lblTime.Caption = ScopeView_ConvertMsToTimeFormattedString(progressPlay.value * cAcc_SampleDeltaT * 1000) & "/" & ScopeView_ConvertMsToTimeFormattedString(progressPlay.Max * cAcc_SampleDeltaT * 1000)
    lblTime.Refresh
End Sub

Public Sub ScopeView_UpdateRecordProgress()
    progressPlay.Max = cScopeMaxNumSamples
    progressPlay.value = Scope_GetDataSampleDistanceFromTail(scopeCurrentPlayedOrRecordedDataSample)
    progressPlay.Text = Format(progressPlay.value / progressPlay.Max * 100, "##0.00") & "%"
    lblTime.Caption = ScopeView_ConvertMsToTimeFormattedString(progressPlay.value * cAcc_SampleDeltaT * 1000) & "/" & ScopeView_ConvertMsToTimeFormattedString(progressPlay.Max * cAcc_SampleDeltaT * 1000)
    lblTime.Refresh
End Sub


Private Function ScopeView_ConvertMsToTimeFormattedString(ms As Long) As String
    Dim minutes As Long
    Dim seconds As Long
    Dim milliseconds As Long
    
    minutes = (ms / 1000) \ 60
    seconds = (ms \ 1000) Mod 60
    milliseconds = ms Mod 1000
    ScopeView_ConvertMsToTimeFormattedString = Format(minutes, "00") & ":" & Format(seconds, "00") & "." & Format(milliseconds, "000")
End Function

Private Sub tmrScopeViewRefresh_Timer()
    If cmdRecStopPlayPause.Caption = "Pause" Or cmdRecStopPlayPause.Caption = "Stop" Then
        ' We are playing or recording. Display the window arround the current recorded or played sample
        ScopeView_SetWindowStartingFromDataSample scopeCurrentPlayedOrRecordedDataSample
    End If
    ScopeView_RedrawAnalyzerData
End Sub



Private Sub ScopeView_AutoCalibrateY()
    Dim minValY As Long
    Dim maxValY As Long
    Dim i As Long
    Dim data As Long
    
    minValY = cScopeMaxDisplayedValue
    maxValY = cScopeMinDisplayedValue
    
    For i = 0 To numDataSamplesInWindow - 1
        If chkShowFilteredAcc(0).value = vbChecked Then
            data = scopeCircularBufferAccDataFiltered(0, Scope_CircularAdd(idxStartDataSampleInWindow, i))
            If data < minValY Then
                minValY = data
            End If
            If data > maxValY Then
                maxValY = data
            End If
        End If
        If chkShowRawAcc(0).value = vbChecked Then
            data = scopeCircularBufferAccDataRaw(0, Scope_CircularAdd(idxStartDataSampleInWindow, i))
            If data < minValY Then
                minValY = data
            End If
            If data > maxValY Then
                maxValY = data
            End If
        End If
    
        If chkShowFilteredAcc(1).value = vbChecked Then
            data = scopeCircularBufferAccDataFiltered(1, Scope_CircularAdd(idxStartDataSampleInWindow, i))
            If data < minValY Then
                minValY = data
            End If
            If data > maxValY Then
                maxValY = data
            End If
        End If
        If chkShowRawAcc(1).value = vbChecked Then
            data = scopeCircularBufferAccDataRaw(1, Scope_CircularAdd(idxStartDataSampleInWindow, i))
            If data < minValY Then
                minValY = data
            End If
            If data > maxValY Then
                maxValY = data
            End If
        End If
    
        If chkShowFilteredAcc(2).value = vbChecked Then
            data = scopeCircularBufferAccDataFiltered(2, Scope_CircularAdd(idxStartDataSampleInWindow, i))
            If data < minValY Then
                minValY = data
            End If
            If data > maxValY Then
                maxValY = data
            End If
        End If
        If chkShowRawAcc(2).value = vbChecked Then
            data = scopeCircularBufferAccDataRaw(2, Scope_CircularAdd(idxStartDataSampleInWindow, i))
            If data < minValY Then
                minValY = data
            End If
            If data > maxValY Then
                maxValY = data
            End If
        End If
    Next
    
    minScopeView = minValY
    maxScopeView = maxValY

    lblScopeViewMax = "Max: " & Format(maxScopeView * 0.06, "##0.00") & " mG"
    lblScopeViewMin = "Min: " & Format(minScopeView * 0.06, "##0.00") & " mG"
End Sub



Private Sub ScopeView_RefreshData()
    Dim acc(3) As Long

    If bPlayedFileIsUsingContainedProcessedData Then
        'Update damped springs
        dampedSpringList(cDampedSpring_FlCabin).length = DampedSpring_ConvertServoLengthToFloatLength(scopeCircularBufferSpringLength(cDampedSpring_FlCabin, scopeCurrentPlayedOrRecordedDataSample))
        dampedSpringList(cDampedSpring_FrCabin).length = DampedSpring_ConvertServoLengthToFloatLength(scopeCircularBufferSpringLength(cDampedSpring_FrCabin, scopeCurrentPlayedOrRecordedDataSample))
        dampedSpringList(cDampedSpring_RCabin).length = DampedSpring_ConvertServoLengthToFloatLength(scopeCircularBufferSpringLength(cDampedSpring_RCabin, scopeCurrentPlayedOrRecordedDataSample))
    Else
        acc(0) = -5.58 / 0.06 'scopeCircularBufferAccDataFiltered(0, scopeCurrentPlayedOrRecordedDataSample)
        acc(1) = 49 / 0.06 'scopeCircularBufferAccDataFiltered(1, scopeCurrentPlayedOrRecordedDataSample)
        acc(2) = 977 / 0.06 'scopeCircularBufferAccDataFiltered(2, scopeCurrentPlayedOrRecordedDataSample)
        DampedSpring_ComputeSpringLength cDampedSpring_FlCabin, acc(2), acc(0), acc(1)
        DampedSpring_ComputeSpringLength cDampedSpring_FrCabin, acc(2), acc(0), acc(1)
        DampedSpring_ComputeSpringLength cDampedSpring_RCabin, acc(2), acc(0), acc(1)
    End If
    
    'Update truck wheels rotation angle
    Truck_UpdateWheelsRotationAngle (scopeCircularBufferTruckWheelsRotationAngle(scopeCurrentPlayedOrRecordedDataSample))
    'Update truck wheels steering angle
    Truck_UpdateWheelsSteeringAngle (scopeCircularBufferTruckWheelsSteeringAngle(scopeCurrentPlayedOrRecordedDataSample))
    'Update the video player
    PlayMovie_RefreshImage
    'Update the play progress
    ScopeView_UpdatePlayProgress
    'Update values of accelerations in the GUI
    ScopeView_UpdateAccLabels
End Sub

