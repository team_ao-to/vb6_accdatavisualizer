Attribute VB_Name = "Module_Draw"
'-----------------------------------------------------------------------------------------------------------------------
'
' (c) Copyright 2016 by Ao-to Corporation. All rights reserved.
'
' The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
' No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
' the specific written permission signed by a legal representative of the Ao-to Corporation.
'
' Change log:
'
' ----------     -----------     -------------     ---------------------------------------------------------------------
' Date           Username        Ticket            Change description
' ----------     -----------     -------------     ---------------------------------------------------------------------
' 19.10.2016     razvan          AOTO2             Initial version of the file
'-----------------------------------------------------------------------------------------------------------------------

' Global constants
Public Const PI = 3.14159265

Public Sub Draw_AddShapeWheelToParticle(P As particle_t, numSegments As Integer, innerRadius As Double, outterRadius As Double, width As Double, color As ColorConstants)
    Dim iCount As Integer, jCount As Integer
    Dim firstPoint(3) As Boolean
    Dim curPointX(3) As Double, curPointY(3) As Double
    Dim firstPointX(3) As Double, firstPointY(3) As Double
    Dim lastPointX(3) As Double, lastPointY(3) As Double
    Dim radius(3) As Double
    Dim zOffset(3) As Double
    
    Dim valCos As Double, valSin As Double
    
    ' Set flag to know that we never computed any point so far
    firstPoint(0) = True
    firstPoint(1) = True
    firstPoint(2) = True
    
    radius(0) = outterRadius
    radius(1) = outterRadius
    radius(2) = innerRadius
    
    zOffset(0) = width / 2
    zOffset(1) = 0
    zOffset(2) = 0
    
    For iCount = 0 To numSegments - 1
        valCos = Cos(iCount * 2 * PI / numSegments)
        valSin = Sin(iCount * 2 * PI / numSegments)
        
        For jCount = 1 To 2
            curPointX(jCount) = radius(jCount) * valCos
            curPointY(jCount) = radius(jCount) * valSin
            If firstPoint(jCount) = True Then
                firstPointX(jCount) = curPointX(jCount)
                firstPointY(jCount) = curPointY(jCount)
                firstPoint(jCount) = False
            Else
                ' Draw line to the previous point
                Draw_AddShapeLineToParticle P, color, curPointX(jCount), curPointY(jCount), zOffset(jCount), lastPointX(jCount), lastPointY(jCount), zOffset(jCount)
                ' Check if this is the last point on the circle
                If (iCount = (numSegments - 1)) Then
                    ' Draw the last line
                    Draw_AddShapeLineToParticle P, color, curPointX(jCount), curPointY(jCount), zOffset(jCount), firstPointX(jCount), firstPointY(jCount), zOffset(jCount)
                End If
            End If
            ' Store values of last computed point
            lastPointX(jCount) = curPointX(jCount)
            lastPointY(jCount) = curPointY(jCount)
        Next
    Next
    ' Rotation indicator on the tire
    Draw_AddShapeLineExistPointsToParticle P, color, 1, 3
End Sub

Public Sub Draw_AddShapeCubeToParticle(P As particle_t, length As Double, width As Double, height As Double, offsetLength As Double, offsetHeight As Double, color As ColorConstants)
   'lenght line bottom, left side
   Draw_AddShapeLineToParticle P, color, offsetLength, offsetHeight, -width / 2, offsetLength + length, offsetHeight, -width / 2
   'lenght line bottom, right side
   Draw_AddShapeLineToParticle P, color, offsetLength, offsetHeight, width / 2, offsetLength + length, offsetHeight, width / 2
   'width line bottom, front
   Draw_AddShapeLineToParticle P, color, offsetLength, offsetHeight, -width / 2, offsetLength, offsetHeight, width / 2
   'width line bottom, rear
   Draw_AddShapeLineToParticle P, color, offsetLength + length, offsetHeight, -width / 2, offsetLength + length, offsetHeight, width / 2
   
   'lenght line top, left side
   Draw_AddShapeLineToParticle P, color, offsetLength, offsetHeight + height, -width / 2, offsetLength + length, offsetHeight + height, -width / 2
   'lenght line top, right side
   Draw_AddShapeLineToParticle P, color, offsetLength, offsetHeight + height, width / 2, offsetLength + length, offsetHeight + height, width / 2
   'width line top, front
   Draw_AddShapeLineToParticle P, color, offsetLength, offsetHeight + height, -width / 2, offsetLength, offsetHeight + height, width / 2
   'width line top, rear
   Draw_AddShapeLineToParticle P, color, offsetLength + length, offsetHeight + height, -width / 2, offsetLength + length, offsetHeight + height, width / 2
   
   'corner line, front left
   Draw_AddShapeLineToParticle P, color, offsetLength, offsetHeight, -width / 2, offsetLength, offsetHeight + height, -width / 2
   'corner line, front right
   Draw_AddShapeLineToParticle P, color, offsetLength, offsetHeight, width / 2, offsetLength, offsetHeight + height, width / 2
   
   'corner line, rear left
   Draw_AddShapeLineToParticle P, color, offsetLength + length, offsetHeight, -width / 2, offsetLength + length, offsetHeight + height, -width / 2
   'corner line, rear right
   Draw_AddShapeLineToParticle P, color, offsetLength + length, offsetHeight, width / 2, offsetLength + length, offsetHeight + height, width / 2
End Sub

Public Sub Draw_AddShapeLineToParticle(P As particle_t, color As ColorConstants, posStartX As Double, posStartY As Double, posStartZ As Double, PosEndX As Double, PosEndY As Double, PosEndZ As Double)
    Dim idxPointStart As Integer, idxPointEnd As Integer
    Dim idxDrawableLines As Integer
    ' Check if the start or end point are already in the list of drawable points
    idxPointStart = Draw_AddDrawablePointToParticle(P, posStartX, posStartY, posStartZ)
    idxPointEnd = Draw_AddDrawablePointToParticle(P, PosEndX, PosEndY, PosEndZ)
    
    'Redim list of drawable lines
    idxDrawableLines = UBound(P.drawLines) + 1
    ReDim Preserve P.drawLines(idxDrawableLines)
    P.drawLines(idxDrawableLines).startPoint = idxPointStart
    P.drawLines(idxDrawableLines).endPoint = idxPointEnd
    P.drawLines(idxDrawableLines).color = color
End Sub

Public Sub Draw_AddShapeLineExistPointsToParticle(P As particle_t, color As ColorConstants, idxPointStart As Integer, idxPointEnd As Integer)
    Dim idxDrawableLines As Integer
    'Redim list of drawable lines
    idxDrawableLines = UBound(P.drawLines) + 1
    ReDim Preserve P.drawLines(idxDrawableLines)
    P.drawLines(idxDrawableLines).startPoint = idxPointStart
    P.drawLines(idxDrawableLines).endPoint = idxPointEnd
    P.drawLines(idxDrawableLines).color = color
End Sub

Public Function Draw_AddDrawablePointToParticle(P As particle_t, posX As Double, posY As Double, posZ As Double) As Integer
    Dim idxDrawablePoints As Integer, iCount As Integer
    idxDrawablePoints = UBound(P.drawPoints)
    
    For iCount = 1 To idxDrawablePoints
        If (P.drawPoints(iCount).offset.X = posX And P.drawPoints(iCount).offset.Y = posY And P.drawPoints(iCount).offset.Z = posZ) Then
            ' Return index of found point
            Draw_AddDrawablePointToParticle = iCount
            Exit Function
        End If
    Next
    'Point was not found. Add in the array of drawable points
    idxDrawablePoints = idxDrawablePoints + 1
    ReDim Preserve P.drawPoints(idxDrawablePoints)
    P.drawPoints(idxDrawablePoints).offset.X = posX
    P.drawPoints(idxDrawablePoints).offset.Y = posY
    P.drawPoints(idxDrawablePoints).offset.Z = posZ
    ' Return index of newly added point
    Draw_AddDrawablePointToParticle = idxDrawablePoints
End Function


Function Draw_GetAngle(X As Double, Y As Double) As Double
    Dim PI As Double
    PI = 4 * Atn(1)

    If X < 0 Then
        '!!!!!Atn gives an angle from the ratio(X/Y) between the x
        '!!!!!and y coordinate of a point.
        Draw_GetAngle = Atn(Y / X) + PI
    ElseIf X > 0 Then
        Draw_GetAngle = Atn(Y / X)
    Else
        If Y < 0 Then
            Draw_GetAngle = 1.5 * PI
        Else
            Draw_GetAngle = 0.5 * PI
        End If
    End If
End Function

Public Sub Draw_AllParticles()
    Dim i As Integer
    
    Dim point As vector_t
    Dim iCount As Integer, jCount As Integer
    Dim X As Double, Y As Double, Z As Double
    Dim NewX As Double, NewY As Double, NewZ As Double
    Dim HypX As Double, HypY As Double, HypZ As Double
    Dim AngX As Double, AngY As Double, AngZ As Double
        
    ' Clear screen
    frmTruck3DView.Cls
    
    ' Loop through all the particles
    For iCount = 1 To UBound(aParticleList)
        ' Loop through all the drawable points of the particle
        For jCount = 1 To UBound(aParticleList(iCount).drawPoints)
            On Error Resume Next 'cuts out overflow errors
            
            With aParticleList(iCount).drawPoints(jCount)
                X = .offset.X
                Y = .offset.Y
                Z = .offset.Z
                'compute the x, y and z coordinates of each point of the shape
                'based on the x, y, z offsets to center of mass and also based
                'on the rotation of center of mass on all 3 axis
                
                
                ' rotation over z axis
                NewX = X * aParticleList(iCount).cosRotAngle.Z - Y * Sin(aParticleList(iCount).rotationAngle.Z)
                NewY = X * Sin(aParticleList(iCount).rotationAngle.Z) + Y * aParticleList(iCount).cosRotAngle.Z
                X = NewX
                Y = NewY
                
                
                ' rotation over x axis
                NewY = Y * Cos(aParticleList(iCount).rotationAngle.X) - Z * Sin(aParticleList(iCount).rotationAngle.X)
                NewZ = Y * Sin(aParticleList(iCount).rotationAngle.X) + Z * Cos(aParticleList(iCount).rotationAngle.X)
                Y = NewY
                Z = NewZ
                
                ' rotation over y axis
                NewX = Z * Sin(aParticleList(iCount).rotationAngle.Y) + X * Cos(aParticleList(iCount).rotationAngle.Y)
                NewZ = Z * Cos(aParticleList(iCount).rotationAngle.Y) - X * Sin(aParticleList(iCount).rotationAngle.Y)
                X = NewX
                Z = NewZ
                

                
                                
                'set the camera at the origin
                X = aParticleList(iCount).posCenterMass.X + X - Camera.posCenterMass.X
                Y = aParticleList(iCount).posCenterMass.Y + Y - Camera.posCenterMass.Y
                Z = aParticleList(iCount).posCenterMass.Z + Z - Camera.posCenterMass.Z
                
                '!!!!!rotate the points
                
                HypX = Sqr(Y ^ 2 + Z ^ 2) 'get the hypotenuse
                AngX = Draw_GetAngle(Z, Y) 'get the angle
            
                Y = Sin(AngX - Camera.rotationAngle.X) * HypX 'rotate the points y coord
                Z = Cos(AngX - Camera.rotationAngle.X) * HypX 'rotate the points z coord
                 

            
                'same
                HypY = Sqr(X ^ 2 + Z ^ 2)
                AngY = Draw_GetAngle(Z, X)
            
                X = Sin(AngY - Camera.rotationAngle.Y) * HypY
                Z = Cos(AngY - Camera.rotationAngle.Y) * HypY
        
                'same
                HypZ = Sqr(X ^ 2 + Y ^ 2)
                AngZ = Draw_GetAngle(Y, X)
            
              
                X = Sin(AngZ - Camera.rotationAngle.Z) * HypZ
                Y = Cos(AngZ - Camera.rotationAngle.Z) * HypZ
            
                ' Update the point's X and Y values in the 2D system (projected one)
                .projectedX = X
                .projectedY = Y
                
                'move the camera to the origin
                If Z > 0 Then
                    'This is the code that prepares the dots for putting on the screen
                    'All you do is divide the x by z and the y by z
                    'See no complex maths involved at all
                    .projectedX = X / Z
                    .projectedY = Y / Z
                End If
              
            End With
        Next
        
        ' 2D coordinates of all drawable points of the shape have been computed
        ' It is time to draw the lines connecting the points
        
        Dim X1 As Double, y1 As Double, X2 As Double, Y2 As Double
        For jCount = 1 To UBound(aParticleList(iCount).drawLines)
            X1 = aParticleList(iCount).drawPoints(aParticleList(iCount).drawLines(jCount).startPoint).projectedX
            y1 = aParticleList(iCount).drawPoints(aParticleList(iCount).drawLines(jCount).startPoint).projectedY
            X2 = aParticleList(iCount).drawPoints(aParticleList(iCount).drawLines(jCount).endPoint).projectedX
            Y2 = aParticleList(iCount).drawPoints(aParticleList(iCount).drawLines(jCount).endPoint).projectedY
            frmTruck3DView.Line (X1, -y1)-(X2, -Y2), aParticleList(iCount).drawLines(jCount).color
        Next
    Next
End Sub


Public Sub Draw_AllSprings()
    On Error Resume Next
    Dim i As Integer
    
    Dim point As vector_t
    Dim iCount As Integer, jCount As Integer
    Dim X As Double, Y As Double, Z As Double
    Dim X1 As Double, y1 As Double, Z1 As Double
    Dim NewX As Double, NewY As Double, NewZ As Double
    Dim HypX As Double, HypY As Double, HypZ As Double
    Dim AngX As Double, AngY As Double, AngZ As Double
        
   
    ' Loop through all the springs
    For iCount = cDampedSpring_FlCabin To cDampedSpring_RCabin
                        
        'set the camera at the origin
        X = dampedSpringList(iCount).basePos.pos.X - Camera.posCenterMass.X
        Y = dampedSpringList(iCount).basePos.pos.Y - Camera.posCenterMass.Y
        Z = dampedSpringList(iCount).basePos.pos.Z - Camera.posCenterMass.Z
        
        X1 = dampedSpringList(iCount).basePos.pos.X - Camera.posCenterMass.X
        'y1 = dampedSpringList(iCount).basePos.pos.Y + dampedSpringList(iCount).length * 100 - Camera.posCenterMass.Y
        y1 = dampedSpringList(iCount).basePos.pos.Y + dampedSpringList(iCount).length / CSng(frmConfigurator.txtSpringMaxLength) * 0.6 * 100 - Camera.posCenterMass.Y
        Z1 = dampedSpringList(iCount).basePos.pos.Z - Camera.posCenterMass.Z
        
        '!!!!!rotate the points
        
        HypX = Sqr(Y ^ 2 + Z ^ 2) 'get the hypotenuse
        AngX = Draw_GetAngle(Z, Y) 'get the angle
    
        Y = Sin(AngX - Camera.rotationAngle.X) * HypX 'rotate the points y coord
        Z = Cos(AngX - Camera.rotationAngle.X) * HypX 'rotate the points z coord
         
         
        HypX = Sqr(y1 ^ 2 + Z1 ^ 2) 'get the hypotenuse
        AngX = Draw_GetAngle(Z1, y1) 'get the angle
    
        y1 = Sin(AngX - Camera.rotationAngle.X) * HypX 'rotate the points y coord
        Z1 = Cos(AngX - Camera.rotationAngle.X) * HypX 'rotate the points z coord
         

    
        'same
        HypY = Sqr(X ^ 2 + Z ^ 2)
        AngY = Draw_GetAngle(Z, X)
    
        X = Sin(AngY - Camera.rotationAngle.Y) * HypY
        Z = Cos(AngY - Camera.rotationAngle.Y) * HypY

        HypY = Sqr(X1 ^ 2 + Z1 ^ 2)
        AngY = Draw_GetAngle(Z1, X1)
    
        X1 = Sin(AngY - Camera.rotationAngle.Y) * HypY
        Z1 = Cos(AngY - Camera.rotationAngle.Y) * HypY


        'same
        HypZ = Sqr(X ^ 2 + Y ^ 2)
        AngZ = Draw_GetAngle(Y, X)
    
      
        X = Sin(AngZ - Camera.rotationAngle.Z) * HypZ
        Y = Cos(AngZ - Camera.rotationAngle.Z) * HypZ
    
        HypZ = Sqr(X1 ^ 2 + y1 ^ 2)
        AngZ = Draw_GetAngle(y1, X1)
    
      
        X1 = Sin(AngZ - Camera.rotationAngle.Z) * HypZ
        y1 = Cos(AngZ - Camera.rotationAngle.Z) * HypZ
    
        'move the camera to the origin
        If Z > 0 Then
            'This is the code that prepares the dots for putting on the screen
            'All you do is divide the x by z and the y by z
            'See no complex maths involved at all
            X = X / Z
            Y = Y / Z
            
            X1 = X1 / Z1
            y1 = y1 / Z1
        End If
              
        
        ' 2D coordinates of all drawable points of the shape have been computed
        ' It is time to draw the lines connecting the points
        
        frmTruck3DView.Line (X, -Y)-(X1, -y1), vbRed
    Next
End Sub



