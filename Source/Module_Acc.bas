Attribute VB_Name = "Module_Acc"
'-----------------------------------------------------------------------------------------------------------------------
'
' (c) Copyright 2016 by Ao-to Corporation. All rights reserved.
'
' The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
' No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
' the specific written permission signed by a legal representative of the Ao-to Corporation.
'
' Change log:
'
' ----------     -----------     -------------     ---------------------------------------------------------------------
' Date           Username        Ticket            Change description
' ----------     -----------     -------------     ---------------------------------------------------------------------
' 19.10.2016     razvan          AOTO2             Initial version of the file
'-----------------------------------------------------------------------------------------------------------------------

' The value of the g constant
Public Const cAcc_g = 9.8 'm/s*s

' We are using all 3 axes of the accelerometer
Public Const cAcc_NumAxes = 3

' The value of one unit received from accelerometer is 0.06 mG
Public Const cAcc_GPerAccMeasuredUnit = 0.00006

' The delta time between acc samples
Public Const cAcc_SampleDeltaT = 0.008  'seconds

' Internal numbers for the acc x, y and z axes
Public Const cAcc_AxisX = 0
Public Const cAcc_AxisY = 1
Public Const cAcc_AxisZ = 2

' Mapping of the acc x, y and z axes over the up-down, front-rear and left-right directions
Public Const cAcc_AxisDown = cAcc_AxisZ
Public Const cAcc_AxisFrontRear = cAcc_AxisX
Public Const cAcc_DownLeftRight = cAcc_AxisY

