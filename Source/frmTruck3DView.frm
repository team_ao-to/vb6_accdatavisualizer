VERSION 5.00
Begin VB.Form frmTruck3DView 
   AutoRedraw      =   -1  'True
   BackColor       =   &H00000000&
   Caption         =   "Truck 3D View"
   ClientHeight    =   5130
   ClientLeft      =   225
   ClientTop       =   570
   ClientWidth     =   7965
   LinkTopic       =   "Form1"
   ScaleHeight     =   5130
   ScaleWidth      =   7965
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer tmr3DViewRefresh 
      Interval        =   50
      Left            =   5040
      Top             =   315
   End
   Begin VB.Label lblParams 
      AutoSize        =   -1  'True
      BackColor       =   &H00000000&
      Caption         =   "lblParams"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   675
   End
End
Attribute VB_Name = "frmTruck3DView"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'-----------------------------------------------------------------------------------------------------------------------
'
' (c) Copyright 2016 by Ao-to Corporation. All rights reserved.
'
' The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
' No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
' the specific written permission signed by a legal representative of the Ao-to Corporation.
'
' Change log:
'
' ----------     -----------     -------------     ---------------------------------------------------------------------
' Date           Username        Ticket            Change description
' ----------     -----------     -------------     ---------------------------------------------------------------------
' 19.10.2016     razvan          AOTO2             Initial version of the file
'-----------------------------------------------------------------------------------------------------------------------

Option Explicit 'make sure we declare all our variables (cuts typing errors)
Dim xMouseDown As Single
Dim yMouseDown As Single

Private Sub Form_Load()
    On Error Resume Next
    Registry_GetFormPosition Me
    ' Read camera rotation angle and radius from registry
    cameraRotationAngle = GetSetting(App.Title, "Settings", Me.Name & "cameraRotationAngle", cCameraRotationAngleInitial)
    cameraRotationRadius = GetSetting(App.Title, "Settings", Me.Name & "cameraRotationRadius", cCameraRotationRadiusInitial)
    Camera_Update
    'Set the centre of the screen to 0,0
    Me.ScaleHeight = 2
    Me.ScaleWidth = 3
    Me.ScaleLeft = Me.ScaleWidth / -2
    Me.ScaleTop = Me.ScaleHeight / -2
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    On Error Resume Next
    Registry_SetFormPosition Me
    ' Store camera rotation angle and radius in registry
    SaveSetting App.Title, "Settings", Me.Name & "cameraRotationAngle", cameraRotationAngle
    SaveSetting App.Title, "Settings", Me.Name & "cameraRotationRadius", cameraRotationRadius
    'Close all app forms
    Main_CloseAllForms
End Sub


Private Sub Form_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 1 Then
        xMouseDown = X
        yMouseDown = Y
    End If
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim Dist As Single
    If Button = 1 Then
        Dist = X - xMouseDown
        xMouseDown = X
        ' Update camera angle
        cameraRotationAngle = cameraRotationAngle + Dist
        Camera_Update
    End If
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyPageUp
            ' Update camera rotation radius
            cameraRotationRadius = cameraRotationRadius - 10
            Camera_Update

        Case vbKeyPageDown
            ' Update camera rotation radius
            cameraRotationRadius = cameraRotationRadius + 10
            Camera_Update
    End Select
End Sub


Private Sub Form_Resize()
    On Error Resume Next
    Me.ScaleHeight = 2
    Me.ScaleWidth = 3
    Me.ScaleLeft = Me.ScaleWidth / -2
    Me.ScaleTop = Me.ScaleHeight / -2
End Sub



Private Sub tmr3DViewRefresh_Timer()
    
    Particle_UpdateCabinPosition
    Draw_AllParticles
    Draw_AllSprings
    Truck3DView_RefreshDisplayedParams
End Sub

Private Sub Truck3DView_RefreshDisplayedParams()
    Dim springFLCompressionPercent As Double, springFRCompressionPercent As Double, springRCompressionPercent As Double
    springFLCompressionPercent = 100 - 100 * (scopeCircularBufferSpringLength(0, scopeCurrentPlayedOrRecordedDataSample) - cServo_MinValUs) / (cServo_MaxValUs - cServo_MinValUs)
    springFRCompressionPercent = 100 - 100 * (scopeCircularBufferSpringLength(1, scopeCurrentPlayedOrRecordedDataSample) - cServo_MinValUs) / (cServo_MaxValUs - cServo_MinValUs)
    springRCompressionPercent = 100 - 100 * (scopeCircularBufferSpringLength(2, scopeCurrentPlayedOrRecordedDataSample) - cServo_MinValUs) / (cServo_MaxValUs - cServo_MinValUs)
    If scopeCurrentPlayedOrRecordedDataSample <> scopeCircularBufferTail Then
        lblParams.Caption = "Spring FL length: " & Format(dampedSpringList(0).length, "##0.00") & vbCrLf & _
                            "Spring FR length: " & Format(dampedSpringList(1).length, "##0.00") & vbCrLf & _
                            "Spring R  length: " & Format(dampedSpringList(2).length, "##0.00") & vbCrLf & _
                            "Truck speed: " & Format(scopeCircularBufferTruckSpeedPercent(scopeCurrentPlayedOrRecordedDataSample), "##0.00") & "%" & vbCrLf & _
                            "Steering angle: " & scopeCircularBufferTruckWheelsSteeringAngle(scopeCurrentPlayedOrRecordedDataSample) & " degrees"

    Else
        lblParams.Caption = ""
    End If
                        
End Sub




