Attribute VB_Name = "Module_Camera"
'-----------------------------------------------------------------------------------------------------------------------
'
' (c) Copyright 2016 by Ao-to Corporation. All rights reserved.
'
' The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
' No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
' the specific written permission signed by a legal representative of the Ao-to Corporation.
'
' Change log:
'
' ----------     -----------     -------------     ---------------------------------------------------------------------
' Date           Username        Ticket            Change description
' ----------     -----------     -------------     ---------------------------------------------------------------------
' 19.10.2016     razvan          AOTO2             Initial version of the file
'-----------------------------------------------------------------------------------------------------------------------

Public Camera As particle_t  'our camera
Public cameraRotationRadius As Double
Public cameraRotationAngle As Double

Public Const cCameraRotationRadiusInitial = 500
Public Const cCameraRotationAngleInitial = -2.0775

Sub Camera_Init()
    Camera.posCenterMass.X = -167
    Camera.posCenterMass.Y = 200
    Camera.posCenterMass.Z = -242
    Camera.rotationAngle.X = 0
    Camera.rotationAngle.Y = 0.9224
    Camera.rotationAngle.Z = 0
    cameraRotationRadius = cCameraRotationRadiusInitial
    cameraRotationAngle = cCameraRotationAngleInitial
End Sub

Sub Camera_Update()
    Camera.posCenterMass.X = cameraRotationRadius * Sin(cameraRotationAngle) + cTruckStructure_DimChassisLength / 2
    Camera.posCenterMass.Z = cameraRotationRadius * Cos(cameraRotationAngle)
    Camera.rotationAngle.Y = 3 + cameraRotationAngle
End Sub

