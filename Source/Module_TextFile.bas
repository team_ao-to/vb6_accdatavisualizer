Attribute VB_Name = "Module_TextFile"
'-----------------------------------------------------------------------------------------------------------------------
'
' (c) Copyright 2016 by Ao-to Corporation. All rights reserved.
'
' The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
' No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
' the specific written permission signed by a legal representative of the Ao-to Corporation.
'
' Change log:
'
' ----------     -----------     -------------     ---------------------------------------------------------------------
' Date           Username        Ticket            Change description
' ----------     -----------     -------------     ---------------------------------------------------------------------
' 19.10.2016     razvan          AOTO2             Initial version of the file
'-----------------------------------------------------------------------------------------------------------------------

Public bPlayedFileIsUsingContainedProcessedData As Boolean


Public Sub TextFile_WriteFile(strFileName As String)
    Dim FSO As Scripting.FileSystemObject
    Dim TextStream As Scripting.TextStream
    Dim strLine As String
    Dim i As Long
    Dim numDataSamples As Long
    Dim curPosInCircularBuffer As Long
    
    'Create a new FileSystemObject
    Set FSO = New Scripting.FileSystemObject

    Set TextStream = FSO.OpenTextFile(strFileName, ForWriting, True)
    'Print header information
    TextStream.WriteLine "Data already processed: " & "true"
    TextStream.WriteLine "Movie offset (in " & (cAcc_SampleDeltaT * 1000) & " ms units): " & CStr(CInt(frmPlayMovie.txtMovieOffset) - 20)
    TextStream.WriteLine "Info only - Spring rest length (m): " & CStr(CSng(frmConfigurator.txtSpringRestLength.Text))
    TextStream.WriteLine "Info only - Spring min length (m):" & CStr(frmConfigurator.Configurator_SpringMinLength)
    TextStream.WriteLine "Info only - Spring max length (m): " & CStr(CSng(frmConfigurator.txtSpringMaxLength.Text))
    TextStream.WriteLine "Info only - Spring stifness: " & CStr(frmConfigurator.Configurator_SpringStifness)
    TextStream.WriteLine "Info only - Spring damping: " & CStr(frmConfigurator.Configurator_SpringDamping)
    TextStream.WriteLine "Info only - Truck cabin mass (kg): " & CStr(frmConfigurator.Configurator_CabinMass)
    numDataSamples = scopeNumDataPointsInCircularBuffers
    TextStream.WriteLine "Num data samples: " & CStr(numDataSamples)
    'Print data information
    For i = 0 To numDataSamples - 1
        curPosInCircularBuffer = Scope_GetTailPosition(i)
        
        strLine = "Data " & CStr(i) & ": "
        ' Add raw acceleration data
        strLine = strLine & CStr(scopeCircularBufferAccDataRaw(cAcc_AxisX, curPosInCircularBuffer)) & "; "
        strLine = strLine & CStr(scopeCircularBufferAccDataRaw(cAcc_AxisY, curPosInCircularBuffer)) & "; "
        strLine = strLine & CStr(scopeCircularBufferAccDataRaw(cAcc_AxisZ, curPosInCircularBuffer)) & "; "
        ' Add filtered acceleration data
        strLine = strLine & CStr(scopeCircularBufferAccDataFiltered(cAcc_AxisX, curPosInCircularBuffer)) & "; "
        strLine = strLine & CStr(scopeCircularBufferAccDataFiltered(cAcc_AxisY, curPosInCircularBuffer)) & "; "
        strLine = strLine & CStr(scopeCircularBufferAccDataFiltered(cAcc_AxisZ, curPosInCircularBuffer)) & "; "
        ' Add springs length
        strLine = strLine & CStr(scopeCircularBufferSpringLength(cDampedSpring_FlCabin, curPosInCircularBuffer)) & "; "
        strLine = strLine & CStr(scopeCircularBufferSpringLength(cDampedSpring_FrCabin, curPosInCircularBuffer)) & "; "
        strLine = strLine & CStr(scopeCircularBufferSpringLength(cDampedSpring_RCabin, curPosInCircularBuffer)) & "; "
        ' Add truck speed
        strLine = strLine & CStr(scopeCircularBufferTruckSpeedPercent(curPosInCircularBuffer)) & "; "
        ' Add truck wheels rotation angle
        strLine = strLine & CStr(scopeCircularBufferTruckWheelsRotationAngle(curPosInCircularBuffer)) & "; "
        ' Add truck wheels steering angle
        strLine = strLine & CStr(scopeCircularBufferTruckWheelsSteeringAngle(curPosInCircularBuffer)) & "; "
        
        TextStream.WriteLine strLine
    Next
    Call TextStream.Close
    Set myTextStream = Nothing
    Set myFSO = Nothing
    
    MsgBox "File successfully saved!", vbOKOnly + vbInformation, "Save successful"
End Sub
    
    
Public Sub TextFile_ReadFile(strFileName As String)
    Dim FSO As Scripting.FileSystemObject
    Dim TextStream As Scripting.TextStream
    
    Dim strLine As String
    Dim numDataSamples As Long
    Dim i As Long
        
    Dim movieOffset As Integer
    Dim configSpringRestLength As Double
    Dim configSpringMinLength As Double
    Dim configSpringMaxLength As Double
    Dim configSpringStifness As Double
    Dim configSpringDamping As Double
    Dim configTruckMass As Double
    
    bPlayedFileIsUsingContainedProcessedData = False
    
    'Create a new FileSystemObject
    Set FSO = New Scripting.FileSystemObject

    'Make sure file exists:
    If Not FSO.FileExists(strFileName) Then
        MsgBox "Cannot read selected file! Please retry !", vbOKOnly + vbCritical, "Error"
    Else
        Set TextStream = FSO.OpenTextFile(strFileName, ForReading)
        ' Start reading the header
        strLine = TextStream.ReadLine
        strLine = Trim(Mid(strLine, InStr(1, strLine, ":") + 1))
        If LCase(strLine) = "true" Then
            res = MsgBox("The file you are importing already contains processed data. " & _
                         "You can just display it or you can reprocess the raw acceleration " & _
                         "data using your configuration data and algorithms local to this machine. Please select your option:" & vbCrLf & _
                         "  Yes - Use processed data existing already in the file." & vbCrLf & _
                         "  No  - Reprocess the raw data using your own configuration data and algorithms.", vbYesNo + vbInformation, "Option selection")
            If res = vbYes Then
                bPlayedFileIsUsingContainedProcessedData = True
            End If
        End If
        
        strLine = TextStream.ReadLine
        movieOffset = CInt(Trim(Mid(strLine, InStr(1, strLine, ":") + 1)))
        frmPlayMovie.txtMovieOffset = CStr(movieOffset)
        ' Skip the info only data
        ' SpringRestLength
        strLine = TextStream.ReadLine
        
        ' SpringMinLength
        strLine = TextStream.ReadLine
        
        ' SpringMaxLength
        strLine = TextStream.ReadLine
        
        ' SpringStifness
        strLine = TextStream.ReadLine
        
        ' SpringDamping
        strLine = TextStream.ReadLine
        ' Truck mass
        strLine = TextStream.ReadLine
        
        ' Read next line: "Num data samples"
        strLine = TextStream.ReadLine
        numDataSamples = CLng(Trim(Mid(strLine, InStr(1, strLine, ":") + 1)))
        frmScopeView.progressPlay.Max = numDataSamples
        frmScopeView.lblTime.Caption = "Loading file data"
        frmScopeView.lblTime.Refresh
        
        For i = 1 To numDataSamples
            strLine = TextStream.ReadLine
            strLine = Trim(Mid(strLine, InStr(1, strLine, ":") + 1))
            If i = 1 Then
                TextFile_ProcessOneInputLine strLine, True
            Else
                TextFile_ProcessOneInputLine strLine, False
            End If
            frmScopeView.progressPlay.value = i
            frmScopeView.progressPlay.Text = Format(i / numDataSamples * 100, "###.00") & "%"
        Next
        Call TextStream.Close
    End If
    Set myTextStream = Nothing
    Set myFSO = Nothing
    'Show window starting with first data sample
    scopeCurrentPlayedOrRecordedDataSample = scopeCircularBufferTail
    frmScopeView.ScopeView_SetWindowStartingFromDataSample scopeCurrentPlayedOrRecordedDataSample
    frmScopeView.HScroll.value = 0
    MsgBox "Successfully read data file. Found " & numDataSamples & " data samples.", vbOKOnly + vbInformation, "Read successful"
End Sub



Private Sub TextFile_ProcessOneInputLine(strLine As String, bThisIsTheFirstLine As Boolean)
    Dim posSeparator As Integer
    Dim accXRaw As Long, accYRaw As Long, accZRaw As Long
    Dim accXFiltered As Long, accYFiltered As Long, accZFiltered As Long
    Dim springLengthFL As Long, springLengthFR As Long, springLengthR As Long
    Dim truckSpeedPercent As Long
    Dim truckWheelsRotationAngle As Single
    Dim truckWheelsSteeringAngle As Long

    posSeparator = InStr(1, strLine, ";")
    accXRaw = CLng(Trim(Left(strLine, posSeparator - 1)))
    strLine = Mid(strLine, posSeparator + 1)
    
    posSeparator = InStr(1, strLine, ";")
    accYRaw = CLng(Trim(Left(strLine, posSeparator - 1)))
    strLine = Mid(strLine, posSeparator + 1)
    
    posSeparator = InStr(1, strLine, ";")
    accZRaw = CLng(Trim(Left(strLine, posSeparator - 1)))
    strLine = Mid(strLine, posSeparator + 1)
    
    posSeparator = InStr(1, strLine, ";")
    accXFiltered = CLng(Trim(Left(strLine, posSeparator - 1)))
    strLine = Mid(strLine, posSeparator + 1)
    
    posSeparator = InStr(1, strLine, ";")
    accYFiltered = CLng(Trim(Left(strLine, posSeparator - 1)))
    strLine = Mid(strLine, posSeparator + 1)
    
    posSeparator = InStr(1, strLine, ";")
    accZFiltered = CLng(Trim(Left(strLine, posSeparator - 1)))
    strLine = Mid(strLine, posSeparator + 1)
    
    posSeparator = InStr(1, strLine, ";")
    springLengthFL = CLng(Trim(Left(strLine, posSeparator - 1)))
    strLine = Mid(strLine, posSeparator + 1)
    
    posSeparator = InStr(1, strLine, ";")
    springLengthFR = CLng(Trim(Left(strLine, posSeparator - 1)))
    strLine = Mid(strLine, posSeparator + 1)
    
    posSeparator = InStr(1, strLine, ";")
    springLengthR = CLng(Trim(Left(strLine, posSeparator - 1)))
    strLine = Mid(strLine, posSeparator + 1)
    
    posSeparator = InStr(1, strLine, ";")
    truckSpeedPercent = CLng(Trim(Left(strLine, posSeparator - 1)))
    strLine = Mid(strLine, posSeparator + 1)
    
    posSeparator = InStr(1, strLine, ";")
    truckWheelsRotationAngle = CSng(Trim(Left(strLine, posSeparator - 1)))
    strLine = Mid(strLine, posSeparator + 1)
    
    posSeparator = InStr(1, strLine, ";")
    truckWheelsSteeringAngle = CLng(Trim(Left(strLine, posSeparator - 1)))
    strLine = Mid(strLine, posSeparator + 1)
    
    If bPlayedFileIsUsingContainedProcessedData = False Then
        ' Need to process the rawData accelerometer using our own filter
        ' MARKER - This code can be replaced with the call to Filter function of another accelerometer data filter
        accXFiltered = AccFilter2_FilterData(cAcc_AxisX, accXRaw)
        accYFiltered = AccFilter2_FilterData(cAcc_AxisY, accYRaw)
        accZFiltered = AccFilter2_FilterData(cAcc_AxisZ, accZRaw)
    End If
    ' Push the read data in the scope circular buffers
    Scope_PushDataInCircularBuffers accXRaw, accYRaw, accZRaw, accXFiltered, accYFiltered, accZFiltered, springLengthFL, springLengthFR, springLengthR, truckSpeedPercent, truckWheelsRotationAngle, truckWheelsSteeringAngle
End Sub

