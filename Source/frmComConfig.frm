VERSION 5.00
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSCOMM32.OCX"
Begin VB.Form frmComConfig 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Serial communication"
   ClientHeight    =   1845
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   4005
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1845
   ScaleWidth      =   4005
   StartUpPosition =   2  'CenterScreen
   Begin MSCommLib.MSComm comPort 
      Left            =   315
      Top             =   1350
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DTREnable       =   -1  'True
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   2295
      TabIndex        =   5
      Top             =   1350
      Width           =   825
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "Connect"
      Height          =   375
      Left            =   3150
      TabIndex        =   4
      Top             =   1350
      Width           =   825
   End
   Begin VB.Frame frame 
      Caption         =   "Select com port"
      Height          =   1170
      Left            =   45
      TabIndex        =   0
      Top             =   45
      Width           =   3915
      Begin VB.CommandButton cmdRefresh 
         Caption         =   "Refresh"
         Height          =   420
         Left            =   2557
         TabIndex        =   2
         Top             =   502
         Width           =   855
      End
      Begin VB.ComboBox cmbComPorts 
         Height          =   315
         Left            =   1125
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   555
         Width           =   1365
      End
      Begin VB.Label Label1 
         Caption         =   "Serial port"
         Height          =   180
         Left            =   180
         TabIndex        =   3
         Top             =   630
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmComConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'-----------------------------------------------------------------------------------------------------------------------
'
' (c) Copyright 2016 by Ao-to Corporation. All rights reserved.
'
' The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
' No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
' the specific written permission signed by a legal representative of the Ao-to Corporation.
'
' Change log:
'
' ----------     -----------     -------------     ---------------------------------------------------------------------
' Date           Username        Ticket            Change description
' ----------     -----------     -------------     ---------------------------------------------------------------------
' 19.10.2016     razvan          AOTO2             Initial version of the file
'-----------------------------------------------------------------------------------------------------------------------

Private Sub Form_Load()
    On Error Resume Next
    Registry_GetFormPosition Me, False
    LoadComPorts
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Registry_SetFormPosition Me, False
End Sub

Private Sub cmdOk_Click()
    SaveSetting App.Title, "Settings", "ComPort", cmbComPorts.Text
    frmScopeView.frameSerial.Visible = True
    If Serial_OpenComPort(cmbComPorts.Text) = True Then
        frmScopeView.lblSerialStatus = "Connected"
        frmScopeView.shapeSerialStatus.FillColor = vbGreen
        frmScopeView.lblSerialStatus.ForeColor = vbGreen
        frmScopeView.shapeSerialStatus.Visible = False
        frmScopeView.cmdRecStopPlayPause.Enabled = True
        frmScopeView.cmdDisconnect.Visible = True
    Else
        frmScopeView.lblSerialStatus = "Disconnected"
        frmScopeView.shapeSerialStatus.FillColor = vbRed
        frmScopeView.lblSerialStatus.ForeColor = vbRed
        frmScopeView.shapeSerialStatus.Visible = True
        frmScopeView.cmdRecStopPlayPause.Enabled = False
    End If
    frmScopeView.ScopeView_ConfigureGUIControls efrmShowReason_RecordFile
    'Unload this form
    Unload Me
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub


Private Sub LoadComPorts()
    On Error Resume Next
    Dim i As Integer
    cmbComPorts.Clear
    For i = 1 To 99
        If OpenComPort(i) = True Then
            cmbComPorts.AddItem "COM " & CStr(i)
        End If
    Next
    If GetSetting(App.Title, "Settings", "ComPort") <> "" Then
        cmbComPorts.Text = GetSetting(App.Title, "Settings", "ComPort")
    End If
End Sub

Private Function OpenComPort(nrPort As Integer) As Boolean
    On Error GoTo Treat_Error
    comPort.CommPort = nrPort
    
    If comPort.PortOpen = True Then
        OpenComPort = False
    Else
        comPort.PortOpen = True
        comPort.PortOpen = False
        OpenComPort = True
    End If
    Exit Function
Treat_Error:

    OpenComPort = False
    
End Function


Private Sub cmdRefresh_Click()
    Dim oldPort As String
    oldPort = cmbComPorts.Text
    LoadComPorts
    For i = 0 To cmbComPorts.ListCount - 1
        If oldPort = cmbComPorts.List(i) Then
            cmbComPorts.ListIndex = i
            Exit For
        End If
    Next
End Sub


