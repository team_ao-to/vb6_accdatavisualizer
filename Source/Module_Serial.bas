Attribute VB_Name = "Module_Serial"
'-----------------------------------------------------------------------------------------------------------------------
'
' (c) Copyright 2016 by Ao-to Corporation. All rights reserved.
'
' The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
' No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
' the specific written permission signed by a legal representative of the Ao-to Corporation.
'
' Change log:
'
' ----------     -----------     -------------     ---------------------------------------------------------------------
' Date           Username        Ticket            Change description
' ----------     -----------     -------------     ---------------------------------------------------------------------
' 19.10.2016     razvan          AOTO2             Initial version of the file
'-----------------------------------------------------------------------------------------------------------------------

Public Serial_NumMissedPackets As Long

Private Const cSerial_NumBytesInCircularRxBuffer = 5000
Private rxBufferHead As Long, rxBufferTail As Long
Private rxBuffer(cSerial_NumBytesInCircularRxBuffer) As Byte
Private numReceivedPackets As Long
Private lastPacketSeqNumber As Single
Private bThisIsTheFirstPacketReceived As Boolean
Private bPushReceivedDataInScopeBuffers As Boolean

'Received messages

Public Const cSerial_DataIndMsgLength = 16
'   2 bytes (0-1)   : Array[2]  - Header
'   1 bytes (2)     : Byte      - MsgType
'   1 bytes (3)     : Byte      - Sequence Number
'   2 bytes (4-5)   : Int       - Filtered Acc X
'   2 bytes (6-7)   : Int       - Filtered Acc Y
'   2 bytes (8-9)   : Int       - Filtered Acc Z
'   2 bytes (10-11) : Int       - Spring Length 0
'   2 bytes (12-13) : Int       - Spring Length 1
'   2 bytes (14-15) : Int       - Spring Length 2
Public Const cSerial_SetAllParamsCnfMsgLength = 4
'   2 bytes (0-1)   : Array[2]  - Header
'   1 bytes (2)     : Byte      - MsgType
'   1 bytes (3)     : Byte      - Status

Public Enum serialMsgType_e
    eSerialMsgType_DataInd = 0
    eSerialMsgType_SetAllParamsReq = 1
    eSerialMsgType_SetAllParamsCnf = 2
End Enum


Function Serial_OpenComPort(strComPort As String) As Boolean
    On Error GoTo TE:
    Serial_OpenComPort = False
    Dim iCount As Long
    
    frmScopeView.comPort.CommPort = val(Mid(strComPort, 4))
    frmScopeView.comPort.InputMode = comInputModeBinary
    frmScopeView.comPort.RThreshold = 1
    frmScopeView.comPort.InputLen = 1
    frmScopeView.comPort.Settings = "115200,N,8,1"
    frmScopeView.comPort.InBufferSize = 32000
    frmScopeView.comPort.InputMode = comInputModeBinary
    frmScopeView.comPort.DTREnable = False
    frmScopeView.comPort.RTSEnable = False
    frmScopeView.comPort.Handshaking = comNone
    frmScopeView.comPort.PortOpen = True
    
    bPushReceivedDataInScopeBuffers = False
    bThisIsTheFirstPacketReceived = True
    Serial_NumMissedPackets = 0
    
    rxBufferHead = 0
    rxBufferTail = 0
    
    Serial_OpenComPort = True
    Exit Function
TE:
    Select Case Err.Number
        Case 8002
            MsgBox "Could not open port " & strComPort & "!" _
            & vbCrLf & "Please select a valid port and retry the operation.", vbExclamation, "Error"

        Case 8005
            MsgBox "Port " & strComPort & " is already open!" _
            & vbCrLf & "Please close the port and retry the operation.", vbExclamation, "Error"
    End Select
End Function

Public Sub Serial_CloseComPort()
    On Error Resume Next
    frmScopeView.comPort.PortOpen = False
End Sub

Public Sub Serial_StartRecordingData()
    bPushReceivedDataInScopeBuffers = True
    bThisIsTheFirstPacketReceived = True
    Serial_NumMissedPackets = 0
End Sub

Public Sub Serial_StopRecordingData()
    bPushReceivedDataInScopeBuffers = False
    Serial_NumMissedPackets = 0
End Sub

Public Sub Serial_RxCallback()
    'On Error Resume Next
    Dim a As Variant
    Dim msgLength As Integer
    
    While frmScopeView.comPort.InBufferCount > 0
        a = frmScopeView.comPort.Input
        Serial_PushDataInCircularRxBuffer a(0)
    Wend
    
    While (Serial_GetSizeOfRxDataInCircularBuffer() >= 2 + 1)
        If (Serial_GetTailValue(0) = 255 And Serial_GetTailValue(1) = 250) Then
            msgLength = Serial_GetMsgLength(Serial_GetTailValue(2))
            If (Serial_GetSizeOfRxDataInCircularBuffer() >= msgLength) Then
                Serial_ProcessRxMessage
                ' Remove processed packet from buffer
                Scope_IncreaseCircularBufferTail (msgLength)
            Else
                Exit Sub
            End If
        Else
            ' Shift the received bytes to the left in the rxBuffer with 1, trying to get to the next start marker
            Scope_IncreaseCircularBufferTail (1)
        End If
    Wend
End Sub

Private Function Serial_GetMsgLength(msgType As Integer)
    Select Case msgType
        Case eSerialMsgType_DataInd
            Serial_GetMsgLength = cSerial_DataIndMsgLength
        Case eSerialMsgType_SetAllParamsCnf
            Serial_GetMsgLength = cSerial_SetAllParamsCnfMsgLength
    End Select
End Function

Private Sub Serial_ProcessRxMessage()
    'On Error Resume Next
    Dim seqNumber As Integer
    Dim rawAccData(3) As Long
    Dim servoSpringLength(3) As Single
    Dim eMsgType As serialMsgType_e
    
    eMsgType = Serial_GetTailValue(2)
    
    Select Case eMsgType
        Case eSerialMsgType_DataInd
        
            '--------------------------------------------------------------
            ' Get the values of the raw accelerations on the 3 axes
            rawAccData(0) = CSng(Serial_GetTailValue(5)) * CSng(256) + _
                             CSng(Serial_GetTailValue(4))
            If rawAccData(0) > 32767 Then
                rawAccData(0) = rawAccData(0) - 65536
            End If
            '--------------------------------------------------------------
            rawAccData(1) = CSng(Serial_GetTailValue(7)) * CSng(256) + _
                             CSng(Serial_GetTailValue(6))
            If rawAccData(1) > 32767 Then
                rawAccData(1) = rawAccData(1) - 65536
            End If
            '--------------------------------------------------------------
            rawAccData(2) = CSng(Serial_GetTailValue(9)) * CSng(256) + _
                             CSng(Serial_GetTailValue(8))
            If rawAccData(2) > 32767 Then
                rawAccData(2) = rawAccData(2) - 65536
            End If
            '--------------------------------------------------------------
            servoSpringLength(0) = CSng(Serial_GetTailValue(11)) * CSng(256) + _
                                   CSng(Serial_GetTailValue(10))
            '--------------------------------------------------------------
            servoSpringLength(1) = CSng(Serial_GetTailValue(13)) * CSng(256) + _
                                   CSng(Serial_GetTailValue(12))
            '--------------------------------------------------------------
            servoSpringLength(2) = CSng(Serial_GetTailValue(15)) * CSng(256) + _
                                   CSng(Serial_GetTailValue(14))
            '--------------------------------------------------------------
            If bPushReceivedDataInScopeBuffers = True Then
                ' MARKER - This code can be replaced with the call to Filter function of another accelerometer data filter
                'accXFiltered = AccFilter2_FilterData(cAcc_AxisX, rawAccData(cAcc_AxisX))
                'accYFiltered = AccFilter2_FilterData(cAcc_AxisY, rawAccData(cAcc_AxisY))
                'accZFiltered = AccFilter2_FilterData(cAcc_AxisZ, rawAccData(cAcc_AxisZ))
                ' Process new spring lengths, only if we have at least 2 packets, so we can compute delta
                'If bThisIsTheFirstPacketReceived = False Then
                    ' Pay attention to the way the x, y, z axes of the accelerometer are mapped over up-down, front-rear and left-right axes
                 '   DampedSpring_ComputeSpringLength cDampedSpring_FlCabin, rawAccData(2), rawAccData(0), rawAccData(1)
                 '   DampedSpring_ComputeSpringLength cDampedSpring_FrCabin, rawAccData(2), rawAccData(0), rawAccData(1)
                 '   DampedSpring_ComputeSpringLength cDampedSpring_RCabin, rawAccData(2), rawAccData(0), rawAccData(1)
                'End If
                dampedSpringList(0).length = DampedSpring_ConvertServoLengthToFloatLength(CLng(servoSpringLength(0)))
                dampedSpringList(1).length = DampedSpring_ConvertServoLengthToFloatLength(CLng(servoSpringLength(1)))
                dampedSpringList(2).length = DampedSpring_ConvertServoLengthToFloatLength(CLng(servoSpringLength(2)))
                
                ' Push the processed data in the scope circular buffers
                Scope_PushDataInCircularBuffers rawAccData(cAcc_AxisX), rawAccData(cAcc_AxisY), rawAccData(cAcc_AxisZ), rawAccData(cAcc_AxisX), rawAccData(cAcc_AxisY), rawAccData(cAcc_AxisZ), CLng(servoSpringLength(0)), CLng(servoSpringLength(1)), CLng(servoSpringLength(2)), 0, 0, 0
                'Update truck wheels rotation angle
                Truck_UpdateWheelsRotationAngle (0)
                'Update truck wheels steering angle
                Truck_UpdateWheelsSteeringAngle (0)
                'Update the play progress
                frmScopeView.ScopeView_UpdateRecordProgress
                frmScopeView.ScopeView_UpdateAccLabels
            End If
            
            ' Process also sequence number
            '--------------------------------------------------------------
            seqNumber = Serial_GetTailValue(3)
            'Check if this is the first packet received
            If bThisIsTheFirstPacketReceived = True Then
                bThisIsTheFirstPacketReceived = False
            Else
                ' Increment with 1 the last received packet seq number
                lastPacketSeqNumber = lastPacketSeqNumber + 1
                If lastPacketSeqNumber = 256 Then
                    lastPacketSeqNumber = 0
                End If
                If seqNumber <> lastPacketSeqNumber Then
                    Serial_NumMissedPackets = Serial_NumMissedPackets + 1
                End If
            End If
            lastPacketSeqNumber = seqNumber
            '--------------------------------------------------------------
            
            'Update status of Rx activity indicator
            numReceivedPackets = numReceivedPackets + 1
            If numReceivedPackets Mod 20 = 0 Then
                frmScopeView.shapeSerialStatus.Visible = Not frmScopeView.shapeSerialStatus.Visible
            End If
        
        Case eSerialMsgType_SetAllParamsCnf
            'Check if status is successful (1) or not (0)
            If Serial_GetTailValue(3) = 1 Then
                frmConfigurator.lblStatus.Caption = "Configuration succesful"
                frmConfigurator.lblStatus.ForeColor = vbBlue
                frmConfigurator.lblStatus.Visible = True
                frmConfigurator.tmrStatus.Enabled = True
            End If
    End Select
    
   
End Sub

Private Function Serial_GetSizeOfRxDataInCircularBuffer() As Long
    If rxBufferHead = rxBufferTail Then
        Serial_GetSizeOfRxDataInCircularBuffer = 0
    ElseIf rxBufferHead > rxBufferTail Then
        Serial_GetSizeOfRxDataInCircularBuffer = rxBufferHead - rxBufferTail
    Else
        Serial_GetSizeOfRxDataInCircularBuffer = cSerial_NumBytesInCircularRxBuffer - (rxBufferTail - rxBufferHead)
    End If
End Function

Private Sub Serial_IncreaseCircularBufferHead(numPositions As Long)
    rxBufferHead = rxBufferHead + numPositions
    If rxBufferHead >= cSerial_NumBytesInCircularRxBuffer Then
        rxBufferHead = rxBufferHead - cSerial_NumBytesInCircularRxBuffer
    End If
End Sub

Private Sub Scope_IncreaseCircularBufferTail(numPositions As Long)
    rxBufferTail = rxBufferTail + numPositions
    If rxBufferTail >= cSerial_NumBytesInCircularRxBuffer Then
        rxBufferTail = rxBufferTail - cSerial_NumBytesInCircularRxBuffer
    End If
End Sub
Private Sub Serial_PushDataInCircularRxBuffer(val As Variant)
    rxBuffer(rxBufferHead) = val
    Serial_IncreaseCircularBufferHead (1)
End Sub

Private Function Serial_GetTailPosition(offsetFromTail As Long) As Long
    Dim pos As Long
    pos = rxBufferTail + offsetFromTail
    If (pos >= cSerial_NumBytesInCircularRxBuffer) Then
        pos = pos - cSerial_NumBytesInCircularRxBuffer
    End If
    Serial_GetTailPosition = pos
End Function

Private Function Serial_GetTailValue(offsetFromTail As Long) As Long
    Serial_GetTailValue = rxBuffer(Serial_GetTailPosition(offsetFromTail))
End Function

