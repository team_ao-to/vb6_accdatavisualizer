Attribute VB_Name = "Module_DampedSpring"
'-----------------------------------------------------------------------------------------------------------------------
'
' (c) Copyright 2016 by Ao-to Corporation. All rights reserved.
'
' The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
' No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
' the specific written permission signed by a legal representative of the Ao-to Corporation.
'
' Change log:
'
' ----------     -----------     -------------     ---------------------------------------------------------------------
' Date           Username        Ticket            Change description
' ----------     -----------     -------------     ---------------------------------------------------------------------
' 19.10.2016     razvan          AOTO2             Initial version of the file
'-----------------------------------------------------------------------------------------------------------------------

Public Type springBasePos_t
    idxParticle As Integer
    offset As vector_t
    jointType As jointType_t
    pos As vector_t
End Type

Public Type dampedSpring_t
    ' Application computed values
    basePos As springBasePos_t
    length As Double
    velocity As Double
End Type

'Springs in the truck
Public Const cDampedSpring_FlCabin = 0
Public Const cDampedSpring_FrCabin = 1
Public Const cDampedSpring_RCabin = 2
Public Const cDampedSpring_FlChassis = 3
Public Const cDampedSpring_FrChassis = 4
Public Const cDampedSpring_RChassis = 5
Public Const cDampedSpring_Max = 6

Public Const cDampedSpring_NumCabinSprings = 3

Private Const cDampedSpring_ChassisSpringDefaultLength = 0.25 'm

' Variables storing the string specific properties
Public dampedSpringList(cDampedSpring_Max) As dampedSpring_t
Public maxSpringLength(cDampedSpring_Max) As Double
Public minSpringLength(3) As Double


Public Sub DampedSpring_InitMinMax()
    Dim i As Integer
    For i = 0 To 3
        minSpringLength(i) = 1000
        maxSpringLength(i) = -1000
    Next

End Sub
'DampedSpring functions
Public Sub DampedSpring_Init()
    Dim i As Integer
    
    For i = 0 To cTruckStructure_SpringMax - 1
        Vector_Reset dampedSpringList(i).basePos.offset
    Next
    DampedSpring_InitMinMax
    ' Init chassis springs
    dampedSpringList(cDampedSpring_FlChassis).length = cDampedSpring_ChassisSpringDefaultLength
    dampedSpringList(cDampedSpring_FrChassis).length = cDampedSpring_ChassisSpringDefaultLength
    
    ' Init cabin springs
    For i = cDampedSpring_FlCabin To cDampedSpring_RCabin
        dampedSpringList(i).length = cDampedSpring_RestLength
        dampedSpringList(i).velocity = 0
        With dampedSpringList(i).basePos
            .pos.X = aParticleList(.idxParticle).posCenterMass.X + .offset.X * aParticleList(.idxParticle).cosRotAngle.Z - .offset.Y * aParticleList(.idxParticle).sinRotAngle.Z
            .pos.Y = aParticleList(.idxParticle).posCenterMass.Y + .offset.X * aParticleList(.idxParticle).sinRotAngle.Z + .offset.Y * aParticleList(.idxParticle).cosRotAngle.Z - .offset.Z * aParticleList(.idxParticle).sinRotAngle.X
            .pos.Z = aParticleList(.idxParticle).posCenterMass.Z + .offset.Z * aParticleList(.idxParticle).cosRotAngle.X + .offset.Y * aParticleList(.idxParticle).sinRotAngle.X
        End With
    Next
End Sub


Public Sub DampedSpring_ComputeSpringLength(idxSpring As Integer, accDown As Long, accFrontRear As Long, accLeftRight As Long)
    Dim cabinForceOverSpring As Double
    Dim springElasticForce As Double
    Dim springTotalForce As Double
    Dim springAcceleration As Double
    Dim springDampingForce As Double
    Dim springLength As Double
    
    Dim adjustedAccUpDown As Double
    Dim adjustedAccFrontRear As Double
    Dim adjustedAccLeftRight As Double
    
    
    adjustedAccUpDown = accDown * cAcc_GPerAccMeasuredUnit
    adjustedAccFrontRear = accFrontRear * cAcc_GPerAccMeasuredUnit
    adjustedAccLeftRight = accLeftRight * cAcc_GPerAccMeasuredUnit
    
    adjustedAccUpDown = Curve_GetValY(curveAccUpDown, adjustedAccUpDown)
    adjustedAccFrontRear = Curve_GetValY(curveAccFrontRear, adjustedAccFrontRear)
    adjustedAccLeftRight = Curve_GetValY(curveAccLeftRight, adjustedAccLeftRight)
    
    ' Compute the force acting on spring, using formula F = m*a for each of the 3 axes
    Select Case idxSpring
        Case cDampedSpring_FrontLeft
            ' Compute force acting on spring due to up-down acceleration
            cabinForceOverSpring = cAcc_g * adjustedAccUpDown * frmConfigurator.Configurator_CabinMass * 1 / 2
            ' Add force acting on spring due to front-rear acceleration
            cabinForceOverSpring = cabinForceOverSpring - cAcc_g * ((cTruckStructure_DimCabinHeight / 2) / cTruckStructure_DimCabinWidth) * adjustedAccFrontRear * frmConfigurator.Configurator_CabinMass / 2
            ' Add force acting on spring due to left-right acceleration
            cabinForceOverSpring = cabinForceOverSpring - cAcc_g * frmConfigurator.Configurator_CabinMass / 2 * adjustedAccLeftRight
        Case cDampedSpring_FrCabin
            ' Compute force acting on spring due to up-down acceleration
            cabinForceOverSpring = cAcc_g * adjustedAccUpDown * frmConfigurator.Configurator_CabinMass * 1 / 2
            ' Add force acting on spring due to front-rear acceleration
            cabinForceOverSpring = cabinForceOverSpring - cAcc_g * ((cTruckStructure_DimCabinHeight / 2) / cTruckStructure_DimCabinWidth) * adjustedAccFrontRear * frmConfigurator.Configurator_CabinMass / 2
            ' Add force acting on spring due to left-right acceleration
            cabinForceOverSpring = cabinForceOverSpring + cAcc_g * frmConfigurator.Configurator_CabinMass / 2 * adjustedAccLeftRight
        Case cDampedSpring_RCabin
            ' Compute force acting on spring due to up-down acceleration
            cabinForceOverSpring = cAcc_g * adjustedAccUpDown * frmConfigurator.Configurator_CabinMass * 1 / 2
            ' Add force acting on spring due to front-rear acceleration
            cabinForceOverSpring = cabinForceOverSpring + cAcc_g * ((cTruckStructure_DimCabinHeight / 2) / cTruckStructure_DimCabinWidth) * adjustedAccFrontRear * frmConfigurator.Configurator_CabinMass / 2
    End Select

    springLength = (CSng(frmConfigurator.txtSpringRestLength.Text) - dampedSpringList(idxSpring).length)
    springElasticForce = springLength * Curve_GetValY(curveSpringStiffness, dampedSpringList(idxSpring).length) 'frmConfigurator.Configurator_SpringStifness
    springDampingForce = frmConfigurator.Configurator_SpringDamping * dampedSpringList(idxSpring).velocity
    springTotalForce = springElasticForce - cabinForceOverSpring - springDampingForce
    
    springAcceleration = springTotalForce / (frmConfigurator.Configurator_CabinMass / 2)
    dampedSpringList(idxSpring).velocity = dampedSpringList(idxSpring).velocity + springAcceleration * cAcc_SampleDeltaT
    dampedSpringList(idxSpring).length = dampedSpringList(idxSpring).length + dampedSpringList(idxSpring).velocity * cAcc_SampleDeltaT


    'Store min and max
    If dampedSpringList(idxSpring).length > maxSpringLength(Index) Then
        maxSpringLength(idxSpring) = dampedSpringList(idxSpring).length
    End If
    If dampedSpringList(idxSpring).length < minSpringLength(idxSpring) Then
        minSpringLength(idxSpring) = dampedSpringList(idxSpring).length
    End If
    
    ' Limit movement of the spring.
    If dampedSpringList(idxSpring).length > CSng(frmConfigurator.txtSpringMaxLength.Text) Then
        dampedSpringList(idxSpring).length = CSng(frmConfigurator.txtSpringMaxLength.Text)
        dampedSpringList(idxSpring).velocity = 0
    End If
    If dampedSpringList(idxSpring).length < frmConfigurator.Configurator_SpringMinLength Then
        dampedSpringList(idxSpring).length = frmConfigurator.Configurator_SpringMinLength
        dampedSpringList(idxSpring).velocity = 0
    End If
End Sub

Public Function DampedSpring_ConvertFloatLengthToServoLength(springLength As Double) As Long
    Dim retVal As Long
    ' retVal = (cServo_MinValUs + (cServo_MaxValUs - cServo_MinValUs) * (springLength - frmConfigurator.Configurator_SpringMinLength) / (csng(frmConfigurator.txtSpringMaxLength.text) - frmConfigurator.Configurator_SpringMinLength))
    ' Changed
    retVal = (cServo_MinValUs + (cServo_MaxValUs - cServo_MinValUs) * (springLength - 0) / (CSng(frmConfigurator.txtSpringRestLength.Text) - 0))
    DampedSpring_ConvertFloatLengthToServoLength = retVal
End Function

Public Function DampedSpring_ConvertServoLengthToFloatLength(servoLength As Long) As Double
    Dim retVal As Double
    retVal = (0 + (CSng(frmConfigurator.txtSpringMaxLength.Text) - 0) * (servoLength - cServo_MinValUs) / (cServo_MaxValUs - cServo_MinValUs))
    DampedSpring_ConvertServoLengthToFloatLength = retVal
End Function


Public Sub DampedSpring_UpdateCabinSpringsBasePositions()
    Dim iCount As Integer
    ' Loop through all the cabin springs
    For iCount = cDampedSpring_FlCabin To cDampedSpring_RCabin
        'Compute the new coordinates of the vector position of the base of the spring
        With dampedSpringList(iCount).basePos
            .pos.X = aParticleList(.idxParticle).posCenterMass.X + .offset.X * aParticleList(.idxParticle).cosRotAngle.Z - .offset.Y * aParticleList(.idxParticle).sinRotAngle.Z
            .pos.Y = aParticleList(.idxParticle).posCenterMass.Y + .offset.X * aParticleList(.idxParticle).sinRotAngle.Z + .offset.Y * aParticleList(.idxParticle).cosRotAngle.Z - .offset.Z * aParticleList(.idxParticle).sinRotAngle.X
            .pos.Z = aParticleList(.idxParticle).posCenterMass.Z + .offset.Z * aParticleList(.idxParticle).cosRotAngle.X + .offset.Y * aParticleList(.idxParticle).sinRotAngle.X
        End With
    Next
End Sub
