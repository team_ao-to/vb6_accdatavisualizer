Attribute VB_Name = "Module_Curve"
'-----------------------------------------------------------------------------------------------------------------------
'
' (c) Copyright 2016 by Ao-to Corporation. All rights reserved.
'
' The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
' No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
' the specific written permission signed by a legal representative of the Ao-to Corporation.
'
' Change log:
'
' ----------     -----------     -------------     ---------------------------------------------------------------------
' Date           Username        Ticket            Change description
' ----------     -----------     -------------     ---------------------------------------------------------------------
' 19.10.2016     razvan          AOTO2             Initial version of the file
'-----------------------------------------------------------------------------------------------------------------------

Public Const cCurve_MaxNumPointsInCurve = 10

Public Type curve_t
    numPoints As Integer
    X(cCurve_MaxNumPointsInCurve) As Single
    Y(cCurve_MaxNumPointsInCurve) As Single
End Type

Public curveAccUpDown As curve_t
Public curveAccFrontRear As curve_t
Public curveAccLeftRight As curve_t
Public curveSpringStiffness As curve_t


Public Sub Curve_Init()
    'Define up-down acceleration curve
    'X: the original value of acceleration as it is output by the filter
    'Y: adjusted value of acceleration
    curveAccUpDown.numPoints = 3
    curveAccUpDown.X(0) = 0.3
    curveAccUpDown.Y(0) = -3 '-5
    curveAccUpDown.X(1) = 1
    curveAccUpDown.Y(1) = 1
    curveAccUpDown.X(2) = 1.7
    curveAccUpDown.Y(2) = 7 '10

    'Define front-rear acceleration curve
    'X: the original value of acceleration as it is output by the filter
    'Y: adjusted value of acceleration
    curveAccFrontRear.numPoints = 3
    curveAccFrontRear.X(0) = -1
    curveAccFrontRear.Y(0) = -10
    curveAccFrontRear.X(1) = 0
    curveAccFrontRear.Y(1) = 0
    curveAccFrontRear.X(2) = 1
    curveAccFrontRear.Y(2) = 10
    
    'Define left-right acceleration curve
    'X: the original value of acceleration as it is output by the filter
    'Y: adjusted value of acceleration
    curveAccLeftRight.numPoints = 3
    curveAccLeftRight.X(0) = -1
    curveAccLeftRight.Y(0) = -5
    curveAccLeftRight.X(1) = 0
    curveAccLeftRight.Y(1) = 0
    curveAccLeftRight.X(2) = 1
    curveAccLeftRight.Y(2) = 5
    
    'curveSpringStiffness.numPoints = 4
    'curveSpringStiffness.X(0) = 0
    'curveSpringStiffness.Y(0) = 10000000
    'curveSpringStiffness.X(1) = 0.1
    'curveSpringStiffness.Y(1) = 250000
    'curveSpringStiffness.X(2) = 0.5
    'curveSpringStiffness.Y(2) = 250000
    'curveSpringStiffness.X(3) = 0.6
    'curveSpringStiffness.Y(3) = -100000
    
    'Define spring stifness curve
    'X: the length of the spring
    'Y: adjusted value of stifnesss
    curveSpringStiffness.numPoints = 2
    curveSpringStiffness.X(0) = 0
    curveSpringStiffness.Y(0) = 1000000
    curveSpringStiffness.X(1) = 0.6
    curveSpringStiffness.Y(2) = -10000
    

End Sub


Public Function Curve_GetValY(curve As curve_t, X As Double) As Double

    Dim i As Integer
    Dim dx As Double, dy As Double
    
    ' Get the point closest to the right of 'X'
    For i = 0 To curve.numPoints - 1
        If curve.X(i) > X Then
            Exit For
        End If
    Next
    ' If X is outside the range defined by the curve, return the values at limits
    If i = 0 Then
        Curve_GetValY = curve.Y(0)
        Exit Function
    ElseIf i = curve.numPoints Then
        Curve_GetValY = curve.Y(curve.numPoints - 1)
        Exit Function
    End If
    ' We're here because X is inside the range defined by the curve
    '  Interpolate between found point and the one before it */
    dx = curve.X(i) - curve.X(i - 1)
    dy = curve.Y(i) - curve.Y(i - 1)
    Curve_GetValY = (X - curve.X(i - 1)) / dx * dy + curve.Y(i - 1)
End Function

