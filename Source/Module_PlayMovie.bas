Attribute VB_Name = "Module_PlayMovie"
'-----------------------------------------------------------------------------------------------------------------------
'
' (c) Copyright 2016 by Ao-to Corporation. All rights reserved.
'
' The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
' No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
' the specific written permission signed by a legal representative of the Ao-to Corporation.
'
' Change log:
'
' ----------     -----------     -------------     ---------------------------------------------------------------------
' Date           Username        Ticket            Change description
' ----------     -----------     -------------     ---------------------------------------------------------------------
' 19.10.2016     razvan          AOTO2             Initial version of the file
'-----------------------------------------------------------------------------------------------------------------------

Public Const cPlayMovie_FrameTime = 0.05  'seconds -> 20 frames/sec

Public Sub PlayMovie_RefreshImage()
    Dim currentSample As Long
    Dim nrAccSamplesToUpdateOneMovieFrame As Integer
    nrAccSamplesToUpdateOneMovieFrame = CInt(cPlayMovie_FrameTime / cAcc_SampleDeltaT)
    
    'Update the video player
    currentSample = Scope_CircularSubstract(scopeCurrentPlayedOrRecordedDataSample, scopeCircularBufferTail)
    ' Add the offset
    If frmPlayMovie.txtMovieOffset.Text <> "" Then
        currentSample = currentSample + CInt(frmPlayMovie.txtMovieOffset.Text)
    End If
    'Each sample takes (cAcc_SampleDeltaT * 1000) ms. But the the pictures are taken every 50 ms
    If currentSample >= 0 Then
        If currentSample Mod nrAccSamplesToUpdateOneMovieFrame = 0 Then
            frmPlayMovie.UpdateMovie frmScopeView.scopeViewPlayedFileDir, currentSample \ nrAccSamplesToUpdateOneMovieFrame
        End If
    End If
End Sub
