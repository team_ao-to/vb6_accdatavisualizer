VERSION 5.00
Begin VB.Form frmPlayMovie 
   BackColor       =   &H00000000&
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Video player"
   ClientHeight    =   5445
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   8700
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5445
   ScaleWidth      =   8700
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtMovieOffset 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   2295
      TabIndex        =   1
      Text            =   "0"
      Top             =   90
      Width           =   600
   End
   Begin VB.Label lblMovieOffset 
      BackColor       =   &H00000000&
      Caption         =   "Movie offset (in 10 ms units)"
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   180
      TabIndex        =   0
      Top             =   135
      Width           =   1995
   End
   Begin VB.Image img 
      Height          =   4650
      Left            =   90
      Stretch         =   -1  'True
      Top             =   495
      Width           =   7350
   End
End
Attribute VB_Name = "frmPlayMovie"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'-----------------------------------------------------------------------------------------------------------------------
'
' (c) Copyright 2016 by Ao-to Corporation. All rights reserved.
'
' The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
' No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
' the specific written permission signed by a legal representative of the Ao-to Corporation.
'
' Change log:
'
' ----------     -----------     -------------     ---------------------------------------------------------------------
' Date           Username        Ticket            Change description
' ----------     -----------     -------------     ---------------------------------------------------------------------
' 19.10.2016     razvan          AOTO2             Initial version of the file
'-----------------------------------------------------------------------------------------------------------------------

Const cPlayMovie_FrameTime = 0.05 ' seconds

Private Sub Form_Load()
    Registry_GetFormPosition Me, True
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Registry_SetFormPosition Me, True
    Main_CloseAllForms
End Sub

Private Sub Form_Resize()
    img.width = Me.width - 2 * img.Left - 250
    img.height = Me.height - 2 * img.Top - 150
End Sub

Public Sub UpdateMovie(playedFileDir As String, frameIdx As Integer)
    On Error Resume Next
    img.Picture = LoadPicture(playedFileDir & "\Movie\aoto" & Format(frameIdx, "000") & ".jpg")
    img.Refresh
End Sub

Private Sub txtMovieOffset_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        'Update the video player
        PlayMovie_RefreshImage
    End If
End Sub
