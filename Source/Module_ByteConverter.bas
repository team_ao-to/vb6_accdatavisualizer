Attribute VB_Name = "Module_ByteConverter"
'-----------------------------------------------------------------------------------------------------------------------
'
' (c) Copyright 2016 by Ao-to Corporation. All rights reserved.
'
' The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
' No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
' the specific written permission signed by a legal representative of the Ao-to Corporation.
'
' Change log:
'
' ----------     -----------     -------------     ---------------------------------------------------------------------
' Date           Username        Ticket            Change description
' ----------     -----------     -------------     ---------------------------------------------------------------------
' 19.10.2016     razvan          AOTO2             Initial version of the file
'-----------------------------------------------------------------------------------------------------------------------

Private Type TwoBytes
    b(1) As Byte
End Type

Private Type FourBytes
    b(3) As Byte
End Type

Private Type IntegerValue
    value As Integer
End Type

Private Type LongValue
    value As Long
End Type

Private Type SingleValue
    value As Single
End Type

Private Sub CopyArray(src() As Byte, srcIndex As Integer, dest() As Byte, destIndex As Integer, length As Integer)
    Dim i As Integer
    For i = 0 To length - 1
        dest(destIndex + i) = src(srcIndex + i)
    Next
End Sub

Public Function ByteConverter_GetInteger(b() As Byte, start As Integer) As Integer
    Dim bytes As TwoBytes
    CopyArray b, start, bytes.b, 0, 2
    Dim v As IntegerValue
    LSet v = bytes
    GetInteger = v.value
End Function

Public Sub ByteConverter_SetInteger(b() As Byte, start As Integer, value As Integer)
    Dim v As IntegerValue
    v.value = value
    Dim bytes As TwoBytes
    LSet bytes = v
    CopyArray bytes.b, 0, b, start, 2
End Sub

Public Function ByteConverter_GetLong(b() As Byte, start As Integer) As Long
    Dim bytes As FourBytes
    CopyArray b, start, bytes.b, 0, 4
    Dim v As LongValue
    LSet v = bytes
    GetLong = v.value
End Function

Public Sub ByteConverter_SetLong(b() As Byte, start As Integer, value As Long)
    Dim v As LongValue
    v.value = value
    Dim bytes As FourBytes
    LSet bytes = v
    CopyArray bytes.b, 0, b, start, 4
End Sub

Public Function ByteConverter_GetSingle(b() As Byte, start As Integer) As Single
    Dim bytes As FourBytes
    CopyArray b, start, bytes.b, 0, 4
    Dim v As SingleValue
    LSet v = bytes
    ByteConverter_GetSingle = v.value
End Function

Public Sub ByteConverter_SetSingle(b() As Byte, start As Integer, value As Single)
    Dim v As SingleValue
    v.value = value
    Dim bytes As FourBytes
    LSet bytes = v
    CopyArray bytes.b, 0, b, start, 4
End Sub

